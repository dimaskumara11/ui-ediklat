<?php
	$q_peserta = $DB->from('peserta_diklat')->where(array('id_kelas'=>$id_kelas, 'status'=>0))->order_by('nama', 'asc')->fetch();
	
?>
<div class="table-toolbar">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading"><strong>Komposisi Penilaian</strong></div>
        <div class="panel-body">
					<form action="kelas" method="post" class="f_action" enctype="multipart/form-data">
						<table class="table table-light" border="0">
							<tr>
							<?php
							$q_penilaian = table_get('kelas_penilaian', array('id_kelas'=>$id_kelas), true);
							if($q_kelas[jenis_diklat] == 'K') {
							?>
								<td>
									<div class="form-group">
										<label>RPP</label>
										<div class="input-group input-xsmall">
											<input name="rpp" type="text" class="form-control input-xsmall" aria-describedby="sizing-addon1" value="<?php echo $q_penilaian[rpp]?>"><span class="input-group-addon" id="sizing-addon1">%</span>
										</div>
									</div>
								</td>
								<td>
									<div class="form-group">
										<label>LK</label>
										<div class="input-group input-xsmall">
											<input name="lk" type="text" class="form-control input-xsmall" aria-describedby="sizing-addon1" value="<?php echo $q_penilaian[lk]?>"><span class="input-group-addon" id="sizing-addon1">%</span>
										</div>
									</div>
								</td>
							<?php
							}
							else {
							?>
								<td>
									<div class="form-group">
										<label>Materi</label>
										<div class="input-group input-xsmall">
											<input name="materi" type="text" class="form-control input-xsmall" aria-describedby="sizing-addon1" value="<?php echo $q_penilaian[materi]?>"><span class="input-group-addon" id="sizing-addon1">%</span>
										</div>
									</div>
								</td>
								<td>
									<div class="form-group">
										<label>Seminar</label>
										<div class="input-group input-xsmall">
											<input name="seminar" type="text" class="form-control input-xsmall" aria-describedby="sizing-addon1" value="<?php echo $q_penilaian[seminar]?>"><span class="input-group-addon" id="sizing-addon1">%</span>
										</div>
									</div>
								</td>
								<td>
									<div class="form-group">
										<label>Sikap</label>
										<div class="input-group input-xsmall">
											<input name="sikap" type="text" class="form-control input-xsmall" aria-describedby="sizing-addon1" value="<?php echo $q_penilaian[sikap]?>"><span class="input-group-addon" id="sizing-addon1">%</span>
										</div>
									</div>
								</td>
							<?php
							}
							?>
								<td>
									<div class="form-group">
										<label>Pembagi Nilai Materi</label>
										<div class="input-group input-xsmall">
											<input name="pembagi" type="text" class="form-control input-xsmall" aria-describedby="sizing-addon1" value="<?php echo $q_penilaian[pembagi]?>">
										</div>
									</div>
								</td>
								<td>
									<div class="form-group">
										<br />
										<button type="submit" name="save_komposisi" value="save_komposisi" class="btn btn-primary">Simpan <i class="fa fa-save"></i></button>
									</div>
								</td>
							</tr>
						</table>
						<input type="hidden" name="id_kelas" value="<?php echo $id_kelas?>">
					</form>
				</div>
			</div>
		</div>
	</div>	
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading"><strong>Export / Import Nilai Materi</strong></div>
        <div class="panel-body">
					<div class="col-md-3">
						<span class="label label-danger" style="font-size:18px">1</span><br /><a href="excel_export?id=<?php echo $id_kelas?>" class="btn purple">Download to Excel</a>
					</div>
						<form action="excel_import" method="post" class="f_action" enctype="multipart/form-data">
							<div class="col-md-3">
								<span class="label label-danger" style="font-size:18px">2</span>&nbsp;<br /><br />Edit / Update Nilai di Excel
							</div>
							<div class="col-md-3">
								<span class="label label-danger" style="font-size:18px">3</span>&nbsp;<input type="file" name="file_url" class="form-control" />
							</div>
							<div class="col-md-2">
								<input type="hidden" name="id_kelas" value="<?php echo $id_kelas?>" />
								<input type="hidden" name="action" value="save_file" />
								<span class="label label-danger" style="font-size:18px">4</span>&nbsp;<input type="submit" name="save" class="btn blue" value="Upload & Simpan">
							</div>
						</form>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<?php
			if(empty($status_kelas)){
			?>
			<div class="btn-group">
				<form action="tambah-peserta" method="post" class="f_action"><button type="submit" class="btn sbold green">Tambah Peserta <i class="fa fa-plus"></i></button><input type="hidden" name="action" value="add"><input type="hidden" name="id_kelas" value="<?php echo $id_kelas?>"></form>
			</div>
			<?php
			}
			else {
			?>
			<div class="btn-group">
				<div class="btn sbold red">Tambah Peserta <i class="fa fa-lock"></i></div>
			</div>
			<?php
			}
			?>
			<form action="kelas" method="post" class="f_action" enctype="multipart/form-data">
				<?php
				if(!empty($q_peserta)){
				?>
				<div class="btn-group">
					<button type="submit" name="save_nilai" value="save_nilai" class="btn btn-primary">Simpan <i class="fa fa-save"></i></button>
				</div>
				<?php
				}
				?>
			</div>
		</div>
	</div>
	<?php
	if(empty($q_peserta)){
		echo '<tr><td>Belum ada Peserta</td></tr>';
	}
	else{
	?>
	<div class="table-scrollable">
	<table class="table table-striped table-bordered table-hover">
		<thead>
			<tr>
				<th>No</th>
				<th>NIP/KTP</th>
				<th>NAMA Peserta</th>
				<?php
					foreach($modul_inti as $dt){
						if($dt[id_modul] == '6' OR $dt[id_modul] == '7'){
						}
						else {
							$nm_modul = $DB->from('diklat_modul')->where(array('id_modul'=>$dt[id_modul]))->fetch_first();
							// echo '<th><div title="'.$nm_modul[nama_modul].'">'.substr($nm_modul[nama_modul], 0, 7).'..</div></th>';
							echo '<td><small>'.str_replace(' ', '<br/>', substr($nm_modul[nama_modul], 0, 50)).'</small></td>';
						}
					}
				?>
				<th>Rata-Rata<br />Materi (<?php echo $q_penilaian[pembagi]?>)</th>
				<?php
				if($q_kelas[jenis_diklat] == 'K') {
				?>
					<th>R.P.P. <?php echo $q_penilaian[rpp]?>%</th>
					<th>L.K. <?php echo $q_penilaian[lk]?>% &nbsp;&nbsp;&nbsp;&nbsp;</th>
				<?php 
				}
				else {
				?>
					<th>Materi <?php echo $q_penilaian[materi]?>%</th>
					<th>Nilai Sikap<br /><?php echo $q_penilaian[sikap]?>%</th>
				<?php
				}
					if($q_penilaian['seminar'] > 0) {
						echo '<th>Seminar '.$q_penilaian[seminar].'%</th>';
					}
				?>
				<th>Nilai Akhir</th>
				<th>Nilai HER</th>
				<th>Status Kelulusan</th>
				<th>Aksi</th>
				<th>NAMA Peserta</th>
			</tr>
		</thead>
		<tbody>
			<?php
				$no = 1;
				foreach($q_peserta as $dt){
					if($dt[id_modul] == '6' OR $dt[id_modul] == '7'){
					}
					else {
						$nilai = $DB->from('peserta_test_jawaban')->where(array('id_kelas'=>$id_kelas, 'user_name'=>$dt[noktp]))->fetch_first();
						(empty($nilai)) ? $nil_pg = '0' : $nil_pg = round(($nilai[nilai]/$nilai[total_soal]*10), 2);
					}
					
				?>
				<tr class="<?php echo ($dt[nilai_akhir]<=$CONFIG['nilai_lulus']) ? 'warning' : '' ?>">
					<td><?php echo $no?></td>
					<td nowrap><?php echo $dt[noktp]?></td>
					<td nowrap><a class="" href="peserta-detail?id=<?php echo $dt[id]?>&kelas=<?php echo $id_kelas?>&act=detail"><?php echo ucwords(strtolower($dt[nama]))?></a></td>
					
					<?php
					$tot = 0;
					$m = 1;
					foreach($modul_inti as $dt2){
						if($dt2[id_modul] == '6' OR $dt2[id_modul] == '7'){
						}
						else {
							$nil = json_decode($dt[nil_pg], true);						
							$id_modul = $dt2[id_modul_jp];
							$tot += $nil[$id_modul];
							$m++;
						}
						if($dt2[id_modul] == '6' OR $dt2[id_modul] == '7'){
						}
						else {
						?>
							<td>
								<!-- <input class="form-control" size="1" type="text" name="nil_<?php echo $id_modul.'_'.$dt[id]?>" id="nil_<?php echo $id_modul.'_'.$dt[id]?>" value="<?php echo empty($nil[$id_modul]) ? '0' : $nil[$id_modul]?>" min="0" max="100" onchange="handleChange(this); avg<?php echo $dt[id]?>()" /> -->
								<?php echo empty($nil[$id_modul]) ? '0' : $nil[$id_modul]?>
							</td>
						<?php
						}
					}
					?>
					<td><input class="form-control" size="1" type="text" id="materi_<?php echo $dt[id]?>" name="materi_<?php echo $dt[id]?>" value="<?php echo number_format(($tot/$q_penilaian[pembagi]), 2, '.', '')?>" min="0" max="100" readonly /></td>
					
					<td><input class="form-control" size="1" type="text" id="rata_<?php echo $dt[id]?>" name="rata_<?php echo $dt[id]?>" value="<?php echo $dt[nil_rata_materi]?>" min="0" max="100" onchange="handleChange(this); sum<?php echo $dt[id]?>()" /></td>
					
					<?php if($q_penilaian['seminar'] > 0) {
						echo '<td><input class="form-control" size="1" type="text" id="seminar_'.$dt[id].'" name="seminar_'.$dt[id].'" value="'.$dt[nil_seminar].'" min="0" max="100" onchange="handleChange(this); sum'.$dt[id].'()" /></td>';
					}
					?>
					<td><input class="form-control" size="1" type="text" id="sikap_<?php echo $dt[id]?>" name="sikap_<?php echo $dt[id]?>" value="<?php echo $dt[nil_sikap]?>" min="0" max="100" onchange="handleChange(this); sum<?php echo $dt[id]?>()" /></td>
					
					<td><input class="form-control" size="1" type="text" id="akhir_<?php echo $dt[id]?>" name="akhir_<?php echo $dt[id]?>" value="<?php echo $dt[nilai_akhir]?>" min="0" max="100" readonly /></td>
					<td><input class="form-control" size="1" type="text" id="her_<?php echo $dt[id]?>" name="her_<?php echo $dt[id]?>" value="<?php echo $dt[nilai_her]?>" min="0" max="100" /></td>
					<td><div class="form-group">
						<select id="status_<?php echo $dt[id]?>" name="status_<?php echo $dt[id]?>" class="bs-select form-control" data-show-subtext="true">
							<option value="0" <?php echo ($dt[status_lulus]==0) ? 'selected' : '' ?>>Pending</option>
							<option value="1" <?php echo ($dt[status_lulus]==1) ? 'selected' : '' ?>>Lulus</option>
							<option value="2" <?php echo ($dt[status_lulus]==2) ? 'selected' : '' ?>>Tidak Lulus</option>
							<option value="3" <?php echo ($dt[status_lulus]==3) ? 'selected' : '' ?>>BATAL</option>
						</select></div></td>
						<td>
							<a class="btn btn-circle btn-sm red tooltips" href="kelas?id_peserta=<?php echo $dt[id]?>&id_kelas=<?php echo $id_kelas?>&action=del"><i class="icon-trash"></i></a>
						</td>
						<td nowrap><a class="" href="peserta-detail?id=<?php echo $dt[id]?>&kelas=<?php echo $id_kelas?>&act=detail"><?php echo $dt[nama]?></a></td>
				</tr>
				<?php
					$no++;
				}
			?>
			</tbody>
		<input type="hidden" name="id_kelas" value="<?php echo $id_kelas?>">
		<input type="hidden" name="id_diklat" value="<?php echo $diklat[id_diklat]?>">
	</table>
	</div>
	<?php
	}
	?>
</form>
<script>
	<?php
	foreach($q_peserta as $dt){
		$total_materi = '';
		echo 'function avg'.$dt[id].'(){';
		foreach($modul_inti as $dt2){
			if($dt2[id_modul] == '6' OR $dt2[id_modul] == '7'){
			}
			else {
				echo 'var nil_'.$dt2[id_modul_jp].'_'.$dt[id].' = document.getElementById(\'nil_'.$dt2[id_modul_jp].'_'.$dt[id].'\').value; ';
			}
		}
		$i = 0;
		foreach($modul_inti as $dt2){
			if($dt2[id_modul] == '6' OR $dt2[id_modul] == '7'){
			}
			else {
				$total_materi .= 'parseInt(nil_'.$dt2[id_modul_jp].'_'.$dt[id].')+';
				$i++;
			}
		}
		echo 'var nil = (('.substr($total_materi, 0, -1).')/'.$q_penilaian[pembagi].')*'.($q_penilaian['materi']/100).'; ';
		
		?>
		document.getElementById('materi_<?php echo $dt[id]?>').value = nil.toFixed(2);
		}
	<?php
		echo 'function sum'.$dt[id].'(){';
		
		if($q_penilaian['seminar'] > 0) {
			echo 'var _seminar = document.getElementById(\'seminar_'.$dt[id].'\').value; ';
			echo 'var seminar = (parseInt(_seminar)*'.($q_penilaian['seminar']/100).'); ';
		}
		echo 'var _rata = document.getElementById(\'rata_'.$dt[id].'\').value; ';
		echo 'var _sikap = document.getElementById(\'sikap_'.$dt[id].'\').value; ';
		if($q_kelas[jenis_diklat] == 'K') {
			echo 'var rata = (parseInt(_rata)*'.($q_penilaian['rpp']/100).'); ';
			echo 'var sikap = (parseInt(_sikap)*'.($q_penilaian['lk']/100).'); ';
		}
		else {
			echo 'var rata = (parseInt(_rata)*'.($q_penilaian['materi']/100).'); ';
			echo 'var sikap = (parseInt(_sikap)*'.($q_penilaian['sikap']/100).'); ';
		}
		if($q_penilaian['seminar'] > 0) {
			echo 'var total = parseInt(rata)+parseInt(seminar)+parseInt(sikap);';
		}
		else {
			echo 'var total = parseInt(rata)+parseInt(sikap);';
		}
		?>
		document.getElementById('akhir_<?php echo $dt[id]?>').value = total.toFixed(2);
		}
	<?php
	}
	?>
	 function handleChange(input) {
    if (input.value < 0) input.value = 0;
    if (input.value > 100) input.value = 100;
  }
</script>