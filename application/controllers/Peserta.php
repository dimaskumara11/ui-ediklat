<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Peserta extends CI_Controller 
{

	public function __construct()
	{
		parent :: __construct();
		$this->load->model('Login_model','login');
		$akses = $this->session->userdata('hak_akses');
		if($akses == 0)
		{
			redirect('Login');
		}
	}

	public function index($id_kelas='')
	{
		if ($id_kelas === '') 
		{
			redirect('Home');
		}
		
		$cek_status = $this->db->get_where('lock_ttd_digital',['id_kelas'=>$id_kelas,'status'=>1])->result();
		$q_kelas = $this->db->query('SELECT
				kelas.id_kelas,
				kelas.id_balai,
				kelas.id_diklat_versi,
				kelas.tgl_mulai,
				kelas.tgl_akhir,
				kelas.status_jadwal,
				kelas.tempat_acara,
				kelas.kelas,
				kelas.status_approve,
				kelas.jenis,
				kelas.jml_peserta,
				kelas.id_diklat_versi,
				m_balai.balai_nama,
				m_balai.balai_alias,
				diklat.id_diklat,
				diklat.nama_diklat,
				diklat.jenis_diklat,
				diklat.tujuan_diklat,
				diklat.bidang_pelatihan,
				diklat_modelpelatihan.nama_model,
				diklat_bidangpelatihan.nama_bidang
			FROM
				kelas
				INNER JOIN diklat ON kelas.id_diklat = diklat.id_diklat
				INNER JOIN m_balai ON kelas.id_balai = m_balai.id_balai
				INNER JOIN diklat_modelpelatihan ON diklat.model_pelatihan = diklat_modelpelatihan.kode_model
				INNER JOIN diklat_bidangpelatihan ON diklat.bidang_pelatihan = diklat_bidangpelatihan.kode_bidang AND
					diklat.jenis_diklat = diklat_bidangpelatihan.kode_jenis
			WHERE
			kelas.id_kelas = '.$id_kelas)->row_array();

		$data["q_kelas"] = $q_kelas;

		$data["q_peserta"] = $this->db->query('SELECT
					peserta_diklat.*,
					kelas.tgl_mulai,
					diklat.nama_diklat,
					m_balai.balai_alias,
					kelas_wi.id_wi
				FROM
					peserta_diklat
					INNER JOIN kelas ON peserta_diklat.id_kelas = kelas.id_kelas
					INNER JOIN diklat ON kelas.id_diklat = diklat.id_diklat
					INNER JOIN m_balai ON peserta_diklat.user_id_balai = m_balai.id_balai
					INNER JOIN kelas_wi ON kelas_wi.id_kelas = kelas.id_kelas
				WHERE 
					kelas.id_kelas = '.$id_kelas.' AND peserta_diklat.status = 0
				GROUP BY
					peserta_diklat.noktp
				ORDER BY
					peserta_diklat.nama')->result_array();
		// print_r($data['q_peserta'][0]['id_wi']);die();

		//kaba_akses
		(count($data["q_peserta"])==count($cek_status)) ? $data['status_approve_kaba'] = 1 : $data['status_approve_kaba'] = 0;

		// kapus akses
		($q_kelas['status_approve']==1) ? $data['status_approve_kapus'] = 1 : $data['status_approve_kapus'] = 0;
		// echo "<pre>";
		// print_r($data["status_approve"]);die();
		$this->load->view('peserta',$data);
	}

	public function versi_2($id_kelas='')
	{
		$cek_status = $this->db->get_where('lock_ttd_digital',['id_kelas'=>$id_kelas,'status'=>1])->result();
		$q_kelas = $this->db->query('SELECT
				kelas.id_kelas,
				kelas.id_balai,
				kelas.id_diklat_versi,
				kelas.tgl_mulai,
				kelas.tgl_akhir,
				kelas.status_jadwal,
				kelas.tempat_acara,
				kelas.kelas,
				kelas.status_approve,
				kelas.jenis,
				kelas.jml_peserta,
				kelas.id_diklat_versi,
				m_balai.balai_nama,
				m_balai.balai_alias,
				diklat.id_diklat,
				diklat.nama_diklat,
				diklat.jenis_diklat,
				diklat.tujuan_diklat,
				diklat.bidang_pelatihan,
				diklat_modelpelatihan.nama_model,
				diklat_bidangpelatihan.nama_bidang
			FROM
				kelas
				INNER JOIN diklat ON kelas.id_diklat = diklat.id_diklat
				INNER JOIN m_balai ON kelas.id_balai = m_balai.id_balai
				INNER JOIN diklat_modelpelatihan ON diklat.model_pelatihan = diklat_modelpelatihan.kode_model
				INNER JOIN diklat_bidangpelatihan ON diklat.bidang_pelatihan = diklat_bidangpelatihan.kode_bidang AND
					diklat.jenis_diklat = diklat_bidangpelatihan.kode_jenis
			WHERE
			kelas.id_kelas = '.$id_kelas)->row_array();

		$data["q_kelas"] = $q_kelas;

		$data["q_peserta"] = $this->db->query('SELECT
					peserta_diklat.*,
					kelas.tgl_mulai,
					diklat.nama_diklat,
					m_balai.balai_alias,
					kelas_wi.id_wi
				FROM
					peserta_diklat
					INNER JOIN kelas ON peserta_diklat.id_kelas = kelas.id_kelas
					INNER JOIN diklat ON kelas.id_diklat = diklat.id_diklat
					INNER JOIN m_balai ON peserta_diklat.user_id_balai = m_balai.id_balai
					INNER JOIN kelas_wi ON kelas_wi.id_kelas = kelas.id_kelas
				WHERE 
					kelas.id_kelas = '.$id_kelas.' AND peserta_diklat.status = 0
				GROUP BY
					peserta_diklat.noktp
				ORDER BY
					peserta_diklat.nama')->result_array();

		// $wi = $this->db->select('*')->join('wi_pengajar','wi_pengajar.id_pengajar = kelas_wi.id_wi')->get_where('kelas_wi',['id_kelas'=>$data["q_peserta"][0]['id_kelas']])->result();
		// print_r($wi);die();

		$tidak_lulus = 0;
		$lulus = 0;
		foreach ($data["q_peserta"] as $key => $value) 
		{
			if($value["status_lulus"]==1)
			{
				$lulus += 1;
			}
			else
			{
				$tidak_lulus += 1;
			}
		}
		$data['lulus'] = $lulus;
		$data['tidak_lulus'] = $tidak_lulus;
		$data['status'] = 1;

		//kaba_akses
		(count($data["q_peserta"])==count($cek_status)) ? $data['status_approve_kaba'] = 1 : $data['status_approve_kaba'] = 0;

		// kapus akses
		($q_kelas['status_approve']==1) ? $data['status_approve_kapus'] = 1 : $data['status_approve_kapus'] = 0;

		if(empty($data["q_peserta"]))
		{
			$this->session->set_flashdata('alert','ada kesalahan');
			redirect("Kelas/versi_2",'refresh');
		}
		$this->load->view('peserta_2',$data);
	}

	public function berada_di_kapus($id_kelas='')
	{
		$cek_status = $this->db->get_where('lock_ttd_digital',['id_kelas'=>$id_kelas,'status'=>1])->result();
		$q_kelas = $this->db->query('SELECT
				kelas.id_kelas,
				kelas.id_balai,
				kelas.id_diklat_versi,
				kelas.tgl_mulai,
				kelas.tgl_akhir,
				kelas.status_jadwal,
				kelas.tempat_acara,
				kelas.kelas,
				kelas.status_approve,
				kelas.jenis,
				kelas.jml_peserta,
				kelas.id_diklat_versi,
				m_balai.balai_nama,
				m_balai.balai_alias,
				diklat.id_diklat,
				diklat.nama_diklat,
				diklat.jenis_diklat,
				diklat.tujuan_diklat,
				diklat.bidang_pelatihan,
				diklat_modelpelatihan.nama_model,
				diklat_bidangpelatihan.nama_bidang
			FROM
				kelas
				INNER JOIN diklat ON kelas.id_diklat = diklat.id_diklat
				INNER JOIN m_balai ON kelas.id_balai = m_balai.id_balai
				INNER JOIN diklat_modelpelatihan ON diklat.model_pelatihan = diklat_modelpelatihan.kode_model
				INNER JOIN diklat_bidangpelatihan ON diklat.bidang_pelatihan = diklat_bidangpelatihan.kode_bidang AND
					diklat.jenis_diklat = diklat_bidangpelatihan.kode_jenis
			WHERE
			kelas.id_kelas = '.$id_kelas)->row_array();

		$data["q_kelas"] = $q_kelas;

		$data["q_peserta"] = $this->db->query('SELECT
					peserta_diklat.*,
					kelas.tgl_mulai,
					diklat.nama_diklat,
					m_balai.balai_alias,
					kelas_wi.id_wi
				FROM
					peserta_diklat
					INNER JOIN kelas ON peserta_diklat.id_kelas = kelas.id_kelas
					INNER JOIN diklat ON kelas.id_diklat = diklat.id_diklat
					INNER JOIN m_balai ON peserta_diklat.user_id_balai = m_balai.id_balai
					INNER JOIN kelas_wi ON kelas_wi.id_kelas = kelas.id_kelas
				WHERE 
					kelas.id_kelas = '.$id_kelas.' AND peserta_diklat.status = 0
				GROUP BY
					peserta_diklat.noktp
				ORDER BY
					peserta_diklat.nama')->result_array();

		// $wi = $this->db->select('*')->join('wi_pengajar','wi_pengajar.id_pengajar = kelas_wi.id_wi')->get_where('kelas_wi',['id_kelas'=>$data["q_peserta"][0]['id_kelas']])->result();
		// print_r($wi);die();

		$tidak_lulus = 0;
		$lulus = 0;
		foreach ($data["q_peserta"] as $key => $value) 
		{
			if($value["status_lulus"]==1)
			{
				$lulus += 1;
			}
			else
			{
				$tidak_lulus += 1;
			}
		}
		$data['lulus'] = $lulus;
		$data['tidak_lulus'] = $tidak_lulus;
		$data['status'] = 0;

		//kaba_akses
		(count($data["q_peserta"])==count($cek_status)) ? $data['status_approve_kaba'] = 1 : $data['status_approve_kaba'] = 0;

		// kapus akses
		($q_kelas['status_approve']==1) ? $data['status_approve_kapus'] = 1 : $data['status_approve_kapus'] = 0;

		if(empty($data["q_peserta"]))
		{
			$this->session->set_flashdata('alert','ada kesalahan');
			redirect("Kelas/versi_2",'refresh');
		}
		$this->load->view('peserta_2',$data);
	}
}
