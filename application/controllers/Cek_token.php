<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cek_token extends CI_Controller 
{

	public function __construct()
	{
		parent :: __construct();
	}

	public function index($token='-')
	{
		$cek_token = $this->db->get_where('ttd_digital_token',['token'=>$token, 'status'=>0])->row();
		// print_r($cek_token);die();
		if(!empty($cek_token))
		{
			$data_sesi = 
			[
				'name_' => 'kaba',
				'hak_akses' => 2,
				'id_pusat' => $cek_token->id_pusat,
				'token' => $token
			];
			$this->session->set_userdata($data_sesi);
			redirect('Kelas/versi_2','refresh');
		}
		redirect('Login');
	}
}
?>