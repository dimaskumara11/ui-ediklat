<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tolak extends CI_Controller 
{

	public function __construct()
	{
		parent :: __construct();
	}

	public function index()
	{

		if($this->session->userdata('hak_akses')==1)
			$user = 'kapus';
		elseif($this->session->userdata('hak_akses')==2)
			$user = 'kaba';

		$id_kelas = $this->input->post('kelas');
		$alasan = $this->input->post('alasan');
		if(empty($alasan) and empty($id_kelas))
		{
			redirect('Kelas');
		}

		if(empty($alasan))
			$alasan = '-';

		$data = 
		[
			'id_kelas' => $id_kelas,
			'status' => 1,
			'user' => $user,
			'keterangan' => 'Tolak',
			'alasan' => $alasan
		];

		$this->db->insert('ttd_digital_history',$data);	

		$update = $this->db->update('kelas',['status_approve'=>1],['id_kelas'=>$id_kelas]);	
		
		$this->session->set_flashdata('alert','penolakan berhasil');
		redirect('Kelas','refresh');
	}
}