<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class History extends CI_Controller 
{

	public function __construct()
	{
		parent :: __construct();
		$this->load->model('Login_model','login');
		$akses = $this->session->userdata('hak_akses');
		if($akses == 0)
		{
			redirect('Login');
		}
	}

	public function index()
	{
		$akses = $this->session->userdata('hak_akses');
		if($akses == 2)
		{
			$token = $this->db->select('id_pusat')->get_where('ttd_digital_token',['token'=>$this->session->userdata('token')])->row();
			$uidbalai = ( ! empty($token) ) ? $token->id_pusat : 0;
		}
		else
		{
			$uidbalai = $this->session->userdata('id_pusat');
		}
			if($uidbalai == 0)
				$where = "";
			elseif($uidbalai == 14)
				$where = " WHERE diklat_bidangpelatihan.id_pusat = 14";
			elseif($uidbalai == 15)
				$where = " WHERE diklat_bidangpelatihan.id_pusat = 15";
			elseif($uidbalai == 16)
				$where = " WHERE diklat_bidangpelatihan.id_pusat = 16";
			else
				$where = " WHERE kelas.id_balai = '$uidbalai' ";
		$data['q_kelas'] = $this->db->query('SELECT
					kelas.id_kelas,
					kelas.id_diklat_versi,
					kelas.tgl_mulai,
					kelas.tgl_akhir,
					kelas.kelas,
					m_balai.id_balai,
					m_balai.balai_nama,
					m_balai.balai_alias,
					diklat.id_diklat,
					diklat.nama_diklat
				FROM
					kelas
					INNER JOIN diklat ON kelas.id_diklat = diklat.id_diklat
					INNER JOIN m_balai ON kelas.id_balai = m_balai.id_balai
					INNER JOIN diklat_bidangpelatihan ON diklat.jenis_diklat = diklat_bidangpelatihan.kode_jenis AND
				diklat.bidang_pelatihan = diklat_bidangpelatihan.kode_bidang
				AND status_approve = 3
				ORDER BY
					kelas.tgls_rencana')->result_array();
		$data['status'] = 0;
		$this->load->view('history_after_approve',$data);
	}
}
