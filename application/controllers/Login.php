<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller 
{

	public function __construct()
	{
		parent :: __construct();
		$this->load->model('Login_model','login');
	}

	public function index()
	{
		$this->load->view('login');
	}

	public function login_post()
	{
		$input = $this->input->post();
		$data = 
		[
			'username' => $input['name'],
			'password' => md5($input['password']),
		];
		$cek = $this->login->cek_login($data);
		// if(empty($eck))
		if(empty($data))
		{
			redirect('Login');
		}
		else
		{
			$data_sesi = 
			[
				'name_' => $input['name'],
				'pass_' => md5($input['password']),
				'email' => $cek->email,
				'pass_email' => md5($cek->pass_email),
				'hak_akses' => $cek->hak_akses,
				'token' => sha1('kapus'),
				'id_pusat'=> $cek->id_pusat
			];
			$this->session->set_userdata($data_sesi);
			redirect('Kelas/versi_2');
		}
	}
}
