<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pdf_sertifikat extends CI_Controller 
{

	public function __construct()
	{
		parent :: __construct();
	}

	public function peserta($id_kelas,$id)
	{
		$id = $this->db->escape_str($id);
		$nilai['nilai_akhir_1'] = '92.5|100'; // sangat memuaskan
		$nilai['nilai_akhir_2'] = '85|92.4'; // memuaskan
		$nilai['nilai_akhir_3'] = '77.5|84.9'; // baik sekali
		$nilai['nilai_akhir_4'] = '70|77.4'; // baik
		$nilai['nilai_akhir_5'] = '70'; // tidak lulus
	$nilai['nilai_akhir_lan_1'] = '90.1|100'; // sangat memuaskan
	$nilai['nilai_akhir_lan_2'] = '80.01|90'; // memuaskan
	$nilai['nilai_akhir_lan_3'] = '70.01|80'; // cuku memuaskan
	$nilai['nilai_akhir_lan_4'] = '60.01|70'; // kurang memuaskan
	$nilai['nilai_akhir_lan_5'] = '60'; // tidak memuaskan
	
	$nilai['sertifikat_lokasi'] = 'Jakarta';
	$nilai['sertifikat_kepala_pim'] = "KEMENTERIAN PUPR";
	$nilai['sertifikat_kepala_pim1'] = "Kepala Badan Pengembangan SDM";
	$nilai['sertifikat_kepala_pim2'] = "Kepala Badan Pengembangan Sumber Daya Manusia";
	$nilai['sertifikat_kepala_teknis'] = "Badan Pengembangan Sumber Daya Manusia";
	$nilai['sertifikat_nama'] = "Ir. LOLLY MARTINA MARTIEF, M.T";
	$nilai['sertifikat_nama_teknis'] = "Ir. Lolly Martina Martief, M.T";
	$nilai['sertifikat_nip'] = "196001101988032001";
	$nilai['sertifikat_niplatsar'] = "NIP. 19600110 198803 2 001";
	$nilai['sertifikat_nippim'] = "NIP. 19600110 198803 2 001";
	
	$nilai['sertifikat_lokasi_lan'] = 'Jakarta';
	$nilai['sertifikat_kepala_lan'] = "LEMBAGA ADMINISTRASI NEGARA RI";
	$nilai['sertifikat_kepala_lan1'] = "Plt. Deputi Bidang Diklat Aparatur ";
	$nilai['sertifikat_nama_lan'] = "Dr. MUHAMMAD TAUFIQ, DEA";
	$nilai['sertifikat_nip_lan'] = "NIP. 19671117 199401 1 001";
		
	$nilai['sertifikat_lokasi_lan_kapus'] = 'Jakarta';
	//$deputi = 'DEPUTI BIDANG DIKLAT APARATUR - LAN RI';
	$nilai['sertifikat_kepala_lan_kapus'] = "Kepala Pusat Pengembangan Program\ndan Pembinaan Diklat";
	//$nilai['deputi'] = 'DEPUTI BIDANG DIKLAT APARATUR LAN RI';
	$nilai['sertifikat_nama_lan_kapus'] = "ERFI MUTHMAINAH, SS., MA";
	$nilai['sertifikat_nip_lan_kapus'] = "NIP. 19710209 199503 2 005";
		
	$nilai['sertifikat_lokasi_kapus'] = 'Jakarta';
	$nilai['sertifikat_kepala_kapus'] = "Kepala Pusat Diklat Manajemen\ndan Pengembangan Jabatan Fungsional";
	$nilai['sertifikat_nama_kapus'] = "Ir. NICODEMUS DAUD, M.Si";
	$nilai['sertifikat_nip_kapus'] = "NIP. 19641230 199703 1 002";

		$this->load->library('Tcpdf');
	$data_peserta = $this->db->query('SELECT
					peserta_diklat.cert
				FROM
					peserta_diklat
					INNER JOIN kelas ON peserta_diklat.id_kelas = kelas.id_kelas
					INNER JOIN diklat ON kelas.id_diklat = diklat.id_diklat
					INNER JOIN m_balai ON peserta_diklat.user_id_balai = m_balai.id_balai
				WHERE 
					kelas.id_kelas = '.$id_kelas.' AND peserta_diklat.status = 0
				GROUP BY
					peserta_diklat.noktp
				ORDER BY
					peserta_diklat.nama')->result();
	// var_dump($data_peserta);die;
	// foreach ($data_peserta as $key => $value) 
	// {
		$q_peserta = $this->db->query("SELECT peserta_diklat.*, peserta_sertifikat.sertifikat as stf FROM peserta_diklat LEFT JOIN peserta_sertifikat ON peserta_diklat.id=peserta_sertifikat.id_peserta WHERE cert = '$id'")->row_array();
		// print_r($q_peserta);
				// Position at 15 mm from bottom
				$q_kelas = $this->db->query('SELECT
					kelas.id_kelas,
					kelas.id_balai,
					kelas.id_diklat_versi,
					kelas.tgl_mulai,
					kelas.tgl_akhir,
					kelas.status_jadwal,
					kelas.tempat_acara,
					kelas.kelas,
					kelas.jenis,
					kelas.jml_peserta,
					kelas.id_diklat_versi,
					m_balai.balai_nama,
					m_balai.balai_alias,
					diklat.id_diklat,
					diklat.nama_diklat,
					diklat.jenis_diklat,
					diklat.tujuan_diklat,
					diklat.bidang_pelatihan,
					diklat_modelpelatihan.nama_model,
					diklat_bidangpelatihan.nama_bidang
				FROM
					kelas
					INNER JOIN diklat ON kelas.id_diklat = diklat.id_diklat
					INNER JOIN m_balai ON kelas.id_balai = m_balai.id_balai
					INNER JOIN diklat_modelpelatihan ON diklat.model_pelatihan = diklat_modelpelatihan.kode_model
					INNER JOIN diklat_bidangpelatihan ON diklat.bidang_pelatihan = diklat_bidangpelatihan.kode_bidang AND
						diklat.jenis_diklat = diklat_bidangpelatihan.kode_jenis
				WHERE
				kelas.id_kelas = '.$id_kelas)->row_array();
				$diklat = $this->db->from('diklat')->where(array('id_diklat'=>$q_kelas["id_diklat"]))->get()->row_array();
			// create new PDF document
			// var_dump($q_peserta);die('bdbjbsjb');
			$pdf = new TCPDF('landscape', PDF_UNIT, 'FOLIO', true, 'UTF-8', false);
			// $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
			$balai_alias = $q_kelas["balai_alias"];
			$nama_diklat = $diklat["nama_diklat"];
			// set document information
			// $pdf->SetCreator(PDF_CREATOR);
			$pdf->SetAuthor('e-Pelatihan');
			$pdf->SetTitle('Sertifikat '.$q_peserta["nama"]);
			$pdf->SetSubject('Sertifikat '.$q_peserta["nama"]);
			// $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
			
			// remove default header/footer
			$pdf->setPrintHeader(false);
			$pdf->setPrintFooter(false);
			
			// margins
			$pdf->SetMargins(0, 0, 0);
			$pdf->SetAutoPageBreak(TRUE, 0);
			
			$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
			
			// set header and footer fonts
			$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
			$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
			
			// set default monospaced font
			$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
			
			// set some language-dependent strings (optional)
			if (@file_exists(dirname(__FILE__).'/lang/eng.php')) 
			{
				require_once(dirname(__FILE__).'/lang/eng.php');
				$pdf->setLanguageArray($l);
			}
			
			$pdf->AddPage();

			if(empty($q_peserta))
			{
				$pdf->SetFont('monotype', '', 24);
				$text = "Maaf, ID Sertifikat SALAH!";
				$pdf->MultiCell(285, 5, $text, 0, 'C', 0, 0, 5, 20, true);
			}
			// elseif($q_peserta[status_peg] == 1){
				// $pdf->SetFont('monotype', '', 24);
				// $text = "Maaf, Anda hanya berhak mendapatkan Surat Keterangan Saja!!";
				// $pdf->MultiCell(285, 5, $text, 0, 'C', 0, 0, 5, 20, true);
			// }
			else 
			{
				$this->load->model('M_trf_send_request');
				$nip = $q_peserta["noktp"];
				$url = "https://simka.pu.go.id/pupr/api/index.php";
				$private_key ='fnaifh84659290';

				$action = 'cv';

				$params = array(
					'action' => $action,
					'nip' => $nip
				);

				$result = $this->M_trf_send_request->trf_send_request($url, $private_key, $params, $action);
				$result = json_decode($result, true);
				// var_dump($result);die;

				if(empty($q_peserta['sertifikat'])):
					$stf = $q_peserta['stf'];
				else:
					$stf = $q_peserta['sertifikat'];
				endif;
					
				$pdf->SetFont('pdfatimes', '', 14);
				$text = "Nomor : ".$stf;
				$pdf->MultiCell(290, 5, $text, 0, 'C', 0, 0, 20, 59, true);
				
				$text = "Badan Pengembangan Sumber Daya Manusia berdasarkan Undang-Undang Nomor 5 Tahun 2014 tentang Aparatur Sipil Negara, serta ketentuan pelaksanaannya, menyatakan bahwa: \n";
				$pdf->MultiCell(290, 5, $text, 0, 'J', 0, 0, 20, 69, true);
				
				// $cekFoto = checkRemoteFile($result["pegawai"]["foto_baru"]);
				//strstr(current(get_headers($url)), "200");
				// $exists = strstr(current(get_headers(  (!empty($result["pegawai"]["foto_baru"])) ? $result["pegawai"]["foto_baru"] : '-' ) ), "404");
				// if($exists == '404 Not Found'){
					$pdf->SetFont('pdfatimes', '', 9);
					$pdf->MultiCell(30, 30, "Pas Photo Warna 4x6\n\nLatar belakang warna merah\n\nPakaian Pria/Wanita baju putih berdasi", 1, 'C', 0, 0, 25, 92, true);
					$pdf->SetFont('pdfatimes', '', 14);
				// }
				// else {
					// $pdf->Image($result["pegawai"]["foto_baru"], 24, 96, 35, 0, '', '', 'T', false, 300, '', false, false, 0, false, false, false);
				// }

				$gol = $this->db->from('r_golongan')->where('kdgolongan', $q_peserta["kdgolongan"])->get()->row_array();
				switch($q_peserta["status_lulus"])
				{
					case '0':
						$status = 'Pending';
						break;
					case '1':
						$status = 'Lulus';
						break;
					case '2':
						$status = 'Tidak Lulus';
						break;
					case '3':
						$status = 'Batal';
						break;
					default:
						$status = 'Mohon Cek Status Kelulusan Peserta';
						break;
				}
				$q_kelas = $this->db->query('SELECT
								kelas.id_kelas,
								kelas.tgl_mulai,
								kelas.tgl_akhir,
								kelas.status_jadwal,
								kelas.tempat_acara,
								kelas.kelas,
								kelas.jenis,
								kelas.jml_peserta,
								kelas.angkatan,
								kelas.id_diklat_versi,
								m_balai.id_balai,
								m_balai.balai_nama,
								m_balai.balai_alias,
								diklat.id_diklat,
								diklat.nama_diklat,
								diklat.jenis_diklat,
								diklat_modelpelatihan.nama_model
							FROM
								kelas
								INNER JOIN diklat ON kelas.id_diklat = diklat.id_diklat
								INNER JOIN m_balai ON kelas.id_balai = m_balai.id_balai
								INNER JOIN diklat_modelpelatihan ON diklat.model_pelatihan = diklat_modelpelatihan.kode_model
							WHERE
								kelas.id_kelas = '.$q_peserta["id_kelas"])->row_array();

					$q_jp = $this->db->query("SELECT sum(jp) as totalJp FROM kelas_wi WHERE id_kelas = '".$q_peserta["id_kelas"]."'")->row_array();
					$jp_ori = $this->db->query("Select SUM(jp) As total From diklat_modul_jp Where parent='".$q_kelas["id_diklat_versi"]."' AND diklat_modul_jp.id_diklat = ".$q_kelas["id_diklat"])->row_array();
					empty($jp_ori["total"]) ? $tot_jp_ori = '0' : $tot_jp_ori = $jp_ori["total"];
					
					$pdf->SetFont('pdfatimes', '', 16);
					$pdf->MultiCell(60, 5, "Nama", 0, 'L', 0, 0, 65, 87, true);
					if($q_peserta['status_peg'] == 1)
						$pdf->MultiCell(60, 6, "NIK", 0, 'L', 0, 0, 65, 93, true);
					else
						$pdf->MultiCell(60, 6, "NIP", 0, 'L', 0, 0, 65, 93, true);
					$pdf->MultiCell(60, 6, "Tempat, Tanggal Lahir", 0, 'L', 0, 0, 65, 99, true);
					$pdf->MultiCell(60, 6, "Pangkat/Golongan", 0, 'L', 0, 0, 65, 106, true);
					$pdf->MultiCell(60, 6, "Jabatan", 0, 'L', 0, 0, 65, 112, true);
					$pdf->MultiCell(60, 6, "Instansi", 0, 'L', 0, 0, 65, 118, true);
					$pdf->MultiCell(60, 6, "Kualifikasi", 0, 'L', 0, 0, 65, 134, true);

					$pdf->SetFont('monotype', '', 16);
					empty($q_peserta["gelara"]) ? $gelara = '' : $gelara = $q_peserta["gelara"].' ';
					$pdf->MultiCell(190, 5, ": ".$gelara.ucwords(strtolower($q_peserta["nama"]))." ".$q_peserta["gelarb"], 0, 'L', 0, 0, 125, 87, true);
					$pdf->MultiCell(190, 6, ": ".$q_peserta["noktp"], 0, 'L', 0, 0, 125, 93, true);
					$pdf->MultiCell(190, 6, ": ".ucwords(strtolower($q_peserta["tempatlahir"])).", ".date_indo($q_peserta["tgllahir"]), 0, 'L', 0, 0, 125, 99, true);
					if($q_peserta["status_peg"] == 1)
						$pdf->MultiCell(190, 6, ": -", 0, 'L', 0, 0, 125, 106, true);
					else
						$pdf->MultiCell(190, 6, ": ".ucwords(strtolower($gol["deskripsi"]))." Gol. ".$gol["golongan"], 0, 'L', 0, 0, 125, 106, true);
					$pdf->SetXY(125, 112);
					$pdf->Cell(189, 6, ": ".htmlspecialchars_decode($q_peserta["jabatan"]), 0, 1, 'L', 0, '', 1);
					$nilai_akhir_1 = explode('|', $nilai["nilai_akhir_1"]);
					$nilai_akhir_2 = explode('|', $nilai["nilai_akhir_2"]);
					$nilai_akhir_3 = explode('|', $nilai["nilai_akhir_3"]);
					$nilai_akhir_4 = explode('|', $nilai["nilai_akhir_4"]);
					$nilai_akhir_5 = $nilai["nilai_akhir_5"];
					if($q_peserta["nilai_akhir"] < $nilai_akhir_5)
						$predikat = 'Tidak Lulus';
					elseif($q_peserta["nilai_akhir"] >= $nilai_akhir_4[0] AND $q_peserta["nilai_akhir"] <= $nilai_akhir_4[1])
						$predikat = 'Baik';
					elseif($q_peserta["nilai_akhir"] >= $nilai_akhir_3[0] AND $q_peserta["nilai_akhir"] <= $nilai_akhir_3[1])
						$predikat = 'Baik Sekali';
					elseif($q_peserta["nilai_akhir"] >= $nilai_akhir_2[0] AND $q_peserta["nilai_akhir"] <= $nilai_akhir_2[1])
						$predikat = 'Memuaskan';
					elseif($q_peserta["nilai_akhir"] >= $nilai_akhir_1[0] AND $q_peserta["nilai_akhir"] <= $nilai_akhir_1[1])
						$predikat = 'Sangat Memuaskan';
					$pdf->SetXY(125, 118);
					$pdf->Cell(179, 6, ": ".$q_peserta["nm_instansi"], 0, 1, 'L', 0, '', 1);
					$pdf->MultiCell(190, 6, ": ".$predikat, 0, 'L', 0, 0, 125, 134, true);
					
					$pdf->SetFont('monotype', '', 16);
					$pdf->MultiCell(190, 6, strtoupper($status), 0, 'L', 0, 0, 130, 126, true);
				
				$q_materi = $this->db->query("SELECT
						kelas_wi.id_modul,
						kelas_wi.id_kelas,
						kelas_wi.tgl_mulai,
						kelas_wi.tgl_selesai,
						kelas_wi.jp,
						diklat_modul.nama_modul
					FROM
						kelas_wi
						INNER JOIN diklat_modul ON kelas_wi.id_modul = diklat_modul.id_modul
					WHERE
						kelas_wi.id_kelas = '".$q_peserta["id_kelas"]."'
					GROUP BY
						kelas_wi.id_modul
					ORDER BY
						kelas_wi.tgl_mulai")->result_array();
				
						$modul = 16;
						$idx = 0;
						$modul_inti = $this->db->from('diklat_modul_jp')->where(array('id_diklat'=>$q_kelas["id_diklat"], 'parent'=>$q_kelas["id_diklat_versi"]))->get()->result_array();

						$no = 1;
						$tJp_ = 0;
						foreach($modul_inti as $dt)
						{
							$nm_modul = $this->db->from('diklat_modul')->where(array('id_modul'=>$dt["id_modul"]))->get()->row_array();
							if($dt["id_modul"] == '6' OR $dt["id_modul"] == '7' OR $dt["id_modul"] == '666' OR $dt["id_modul"] == '1161')
							{
							}
							else {				
								$tJp_ += $dt["jp"];
								$no++;
							}
						}
				
						$pdf->SetFont('pdfatimes', '', 14);
						$pdf->MultiCell(290, 6, "pada kegiatan Pelatihan ".htmlspecialchars_decode($q_kelas["nama_diklat"])." Angkatan ".$q_kelas["angkatan"]." Tahun ".substr($q_kelas["tgl_mulai"], 0, 4)." yang diselenggarakan oleh ".$q_kelas["balai_nama"]." mulai tanggal ".date_indo($q_kelas["tgl_mulai"])." sampai dengan ".date_indo($q_kelas["tgl_akhir"])." di ".$q_kelas["tempat_acara"]." yang meliputi ".$tJp_." jam pelatihan. \n", 0, 'J', 0, 0, 20, 144, true);
						
						$pdf->SetFont('pdfatimes', '', 16);
						$pdf->MultiCell(130, 6, $nilai['sertifikat_lokasi'].", ".date_indo($q_kelas["tgl_akhir"]), 0, 'C', 0, 0, 170, 165, true);
						$pdf->SetFont('pdfatimes', 'B', 16);
						$pdf->MultiCell(130, 6, $nilai['sertifikat_kepala_teknis'], 0, 'C', 0, 0, 170, 170, true);
						$pdf->MultiCell(130, 6, "Kepala,", 0, 'C', 0, 0, 170, 176, true);

//ttd
// $pdf->SetLineStyle(array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(255, 255, 255)));
// $pdf->Image(base_url().'assets/ttd_kb.jpeg', 205, 182, 60, 18, 'JPEG', '', '', true, 150, '', false, false, 1, false, false, false);


						$pdf->SetFont('pdfatimes', 'BU', 16);
						$pdf->MultiCell(130, 6, $nilai['sertifikat_nama_teknis'], 0, 'C', 0, 0, 170, 198, true);
						$pdf->SetFont('pdfatimes', 'B', 16);
						$pdf->MultiCell(130, 6, $nilai['sertifikat_nip'], 0, 'C', 0, 0, 170, 204, true);
						// set style for barcode
						$style = array(
								'border' => 2,
								'vpadding' => 'auto',
								'hpadding' => 'auto',
								'fgcolor' => array(0,0,0),
								'bgcolor' => false, //array(255,255,255)
								'module_width' => 1, // width of a single module in points
								'module_height' => 1 // height of a single module in points
						);

						$pdf->write2DBarcode('https://bpsdm.pu.go.id/pelatihan/cek-sertifikat?id='.$id, 'QRCODE,M', 24, 175, 30, 30, $style, 'N');
						$pdf->SetFont('pdfatimes', '', 10);
						$pdf->Text(21, 205, $id);
						$pdf->SetFont('pdfatimes', 'I', 8);
						$pdf->Text(21, 210, "Scan QR Code diatas atau masuk ke https://bpsdm.pu.go.id/pelatihan/cek-sertifikat?id=".$id." untuk cek keaslian sertifikat ini.");


						$pdf->AddPage();
						$pdf->SetFont('pdfatimes', '', 18);
						$text = "DAFTAR MATA PELAJARAN";
						$pdf->MultiCell(290, 5, $text, 0, 'C', 0, 0, 20, 50, true);

						$row = 1;
						foreach($modul_inti as $dt)
						{
							if($dt["id_modul"] == '6' OR $dt["id_modul"] == '7' OR $dt["id_modul"] == '666' OR $dt["id_modul"] == '1161')
							{
							}
							else 
							{
								$nm_modul = $this->db->from('diklat_modul')->where(array('id_modul'=>$dt["id_modul"]))->get()->row_array();
								$nama_modul[$idx] = htmlspecialchars_decode($nm_modul["nama_modul"]);
								$jp[$idx] = $dt["jp"];
								$row++;
								$idx++;
							}
						}
			
						if($row <= 16)
						{
							$pdf->SetFont('pdfatimes', '', 14);
							$html = '<style>
								.ch {text-align: center; font-size: 16}
								.c {text-align: center;}
							</style>
							<table border="1" cellpadding="3"><tr><td class="ch" style="width:40px">No</td><td class="ch"style="width: 840px" >MATA PELAJARAN</td><td class="ch" style="width:130px">JAM<br/> PELAJARAN</td></tr>';
							// for($row = 1; $row<=$modul; $row++){
							$no = 1;
							$tJp = 0;
							foreach($modul_inti as $dt)
							{
								$nm_modul = $this->db->from('diklat_modul')->where(array('id_modul'=>$dt["id_modul"]))->get()->row_array();
								if($dt["id_modul"] == '6' OR $dt["id_modul"] == '7' OR $dt["id_modul"] == '666' OR $dt["id_modul"] == '1161')
								{
								}
								else 
								{
									$html .= '<tr>
										<td class="c">'.$no.'</td>
										<td nowrap>'.htmlspecialchars_decode($nm_modul["nama_modul"]).'</td>
										<td class="c">'.$dt["jp"].'</td>
									</tr>';
									
									$tJp += $dt["jp"];
									$no++;
								}
							}

							$html .= '<tr><td colspan="2" class="ch">JUMLAH JAM PELAJARAN</td><td class="ch">'.$tJp.'</td></tr></table>';
							$pdf->writeHTMLCell(0, 0, 20, 65, $html, '', 1, 0, true, 'L', true);
						}
						elseif($row > 16)
						{
							$mod = $row%2;
							if($mod == 0)
							{
								$col1 = ($row/2);
							}
							else 
							{
								$col1 = floor(($row/2));
							}
							
							$pdf->SetFont('pdfatimes', '', 11);
							$html = '<style>
								.ch {text-align: center; font-size: 12}
								.c {text-align: center;}
								.s {letter-spacing: -1px;}
							</style>
							<table border="1" cellpadding="3"><tr><td class="ch" style="width:25px; vertical-align: middle;">No</td><td class="ch"style="width: 420px" >MATA PELAJARAN</td><td class="ch" style="width:90px; letter-spacing: -1px;">JAM<br/> PELAJARAN</td></tr>';
							// for($row = 1; $row<=$modul; $row++){
							$no = 1;
							$tJp = 0;
							for($tbl1=0; $tbl1<=$col1; $tbl1++){
								$html .= '<tr>
									<td class="c">'.$no.'</td>
									<td nowrap class="s">'.(empty($nama_modul[$tbl1])) ? '' : $nama_modul[$tbl1].'</td>
									<td class="c">'.(empty($jp[$tbl1])) ? '' : $jp[$tbl1].'</td>
								</tr>';
								
								$tJp += $jp[$tbl1];
								$no++;
							}
							$html .= '</table>';
							$pdf->writeHTMLCell(0, 0, 5, 65, $html, '', 1, 0, true, 'L', true);
							
							$html = '<style>
								.ch {text-align: center; font-size: 12}
								.chm {text-align: center; font-size: 12; height:46px;}
								.c {text-align: center;}
								.s {letter-spacing: -1px;}
							</style>
							<table border="1" cellpadding="3"><tr valign="middle"><td class="ch" style="width:25px">No</td><td class="ch"style="width: 420px">MATA PELAJARAN</td><td class="ch" style="width:90px; letter-spacing: -1px;">JAM<br/> PELAJARAN</td></tr>';
							
							for($tbl1=($col1+1); $tbl1<$row; $tbl1++)
							{
								$html .= '<tr>
									<td class="c">'.$no.'</td>
									<td nowrap class="s">'.$nama_modul[$tbl1].'</td>
									<td class="c">'.$jp[$tbl1].'</td>
								</tr>';
								
								$tJp += $jp[$tbl1];
								$no++;
							}
							$html .= '<tr><td colspan="2" class="chm"><div>JUMLAH JAM PELAJARAN</div></td><td class="ch">'.$tJp.'</td></tr></table>';
							$pdf->writeHTMLCell(0, 0, 165, 65, $html, '', 1, 0, true, 'L', true);
						}

				//foooter

				$pdf->SetY(-23);
				// Set font
				$pdf->SetFont('helvetica', 'IB', 9);
				// Page number
				$pdf->Cell(0, 5, 'Hal. '.$pdf->getAliasNumPage().'/'.$pdf->getAliasNbPages(), 0, false, 'R', 0, '', 0, false, 'T', 'M');
				$pdf->SetFont('helvetica', 'I', 8);
				$pdf->SetY(-19);
				$pdf->Cell(0, 5, $balai_alias.' - '.$nama_diklat.' - Rekap e-Pelatihan', 0, false, 'R', 0, '', 0, false, 'T', 'M');

	
				$pdf->lastPage();
				$pel = str_replace(' ', '-', strtolower(trim($q_peserta["nama"]))).'_'.$q_peserta['noktp'].date('YmdHis').'_'.rand(1000, 9999); //.'-'.str_replace(' ', '-', $q_kelas["nama_diklat"]);
				$save = $pdf->Output('sertifikat-'.$pel, 'I');
				
				// $this->load->helper('string');
				// @mkdir('assets/pdf_digital/'.date('Y').'/'.date('m'), 0777, TRUE);
				// $path = date('Y').'/'.date('m');
				// $dt_file = '<!DOCTYPE html><html><head><title>403 Forbidden</title></head><body><p>Directory access is forbidden.</p></body></html>';
				// if(!file_exists('assets/pdf_digital/'.date('Y').'/'.date('m').'/index.html')){
				// 	$fh = fopen('assets/pdf_digital/'.date('Y').'/'.date('m').'/index.html', 'w+');
				// 	fwrite($fh, $dt_file);
				// }
				// $pdf->Output(FCPATH.'./assets/pdf_digital/'.$path.'/'.$pel.'.pdf', 'F');

				// $inser_path = $path.'/'.$pel.'.pdf';
			}
		// }
	}

}