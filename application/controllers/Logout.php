<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logout extends CI_Controller 
{

	public function __construct()
	{
		parent :: __construct();
	}

	public function index()
	{
		$this->db->update('ttd_digital_token',['status'=>1],['token'=>$this->session->userdata('token')]);
		$this->session->sess_destroy();
		redirect('Login','refresh');
	}

}
