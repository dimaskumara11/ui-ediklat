<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cetak_pdf extends CI_Controller 
{

	public function __construct()
	{
		parent :: __construct();
		$this->load->model('Login_model','login');
	}

	public function index($id_kelas, $offset = 0, $total = 0)
	{
		$data['id_kelas'] = $id_kelas;
		$data['offset'] = $offset;
		$data['total'] = $total;

		$data['peserta'] = 
			$this->db->select('id, noktp')
			->from('lock_ttd_digital')
			->where('id_kelas', $id_kelas)
			->limit(1, $offset)
			->get()->result();

		if($total == 0)
		{
			$_total = 
				$this->db->select('count(*) as total')
				->from('lock_ttd_digital')
				->where('id_kelas', $id_kelas)
				->get()->row();

			$data['total'] = $_total->total;
			
		}
		else 
		{
			if(!empty($peserta)){
				header('Refresh: 1;url='.base_url('cetak_pdf/index/'.$id_kelas.'/'.($offset+1).'/'.$total));
				// print_r($peserta);
			}
			else 
			{
				// echo 'KOSONG';
				header('Refresh: 1;url='.base_url('cetak_pdf/index/'.$id_kelas.'/'.$offset));
			}
		}
		$this->load->view('cetak_pdf_2', $data);

	}
}