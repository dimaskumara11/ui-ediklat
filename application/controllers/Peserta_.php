<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Peserta_ extends CI_Controller 
{

	public function __construct()
	{
		parent :: __construct();
	}
	public function index($id)
	{
		redirect('peserta/index/'.$id,'refresh');
	}
	public function versi_2($id)
	{
		redirect('peserta/versi_2/'.$id,'refresh');
	}
}