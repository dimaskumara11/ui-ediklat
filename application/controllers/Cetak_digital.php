<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cetak_digital extends CI_Controller 
{

	public function __construct()
	{
		parent :: __construct();
		$this->load->model('Login_model','login');
	}

	public function index($id_kelas = null, $offset = 0, $total = 0)
	{

		// $input=$this->input->post();
 
		// $id_kelas = $input['kelas'];
		// $pin = $input['passphrase'];
		$pin = $this->session->userdata('passphrase');

		/////// Tambahan Progres bar ////////
		$data['id_kelas'] = $id_kelas;
		$data['offset'] = $offset;
		$data['total'] = $total;

		$data['peserta'] = 
			$this->db->select('id, noktp')
			->from('lock_ttd_digital')
			->where('id_kelas', $id_kelas)
			->limit(1, $offset)
			->get()->result();

		if($total == 0)
		{
			$_total = 
				$this->db->select('count(*) as total')
				->from('lock_ttd_digital')
				->where('id_kelas', $id_kelas)
				->get()->row();

			$data['total'] = $_total->total;
			
		}
		else 
		{
			if(!empty($peserta)){

				// Code Old //
				$q_peserta = $this->db->query('SELECT
					id_kelas,noktp,nama
				FROM
					peserta_diklat
				WHERE 
					id_kelas = '.$id_kelas.' AND peserta_diklat.status = 0
				GROUP BY
					peserta_diklat.noktp
				ORDER BY
					peserta_diklat.nama limit 1')->result();
		
				$nama_peserta = array();
				foreach ($q_peserta as $key => $value) 
				{
						$file = $this->db->order_by('id','DESC')->get_where('lock_ttd_digital',['id_kelas'=>$id_kelas, 'noktp'=>$value->noktp])->row();
						// print_r($file);die();
						$path_jar = './Modul_BSrE/jsignpdf_library/JSignPdf.jar';

						$path_pdf = './assets/pdf_digital/'.$file->file_pdf;
						// print_r($path_pdf);die();
						$path_p12 = './Modul_BSrE/maret_2.p12';
						// print_r($path_p12);die();
						// coba1234.! develope
						$passphrase = $pin;

							$this->load->helper('string');
							@mkdir('assets/pdf_digital/hasil/'.date('Y').'/'.date('m').'/'.$id_kelas, 0777, TRUE);
							$path = date('Y').'/'.date('m').'/'.$id_kelas;
							$dt_file = '<!DOCTYPE html><html><head><title>403 Forbidden</title></head><body><p>Directory access is forbidden.</p></body></html>';
							if(!file_exists('assets/pdf_digital/hasil/'.date('Y').'/'.date('m').'/'.$id_kelas.'/index.html')){
								$fh = fopen('assets/pdf_digital/hasil/'.date('Y').'/'.date('m').'/'.$id_kelas.'/index.html', 'w+');
								fwrite($fh, $dt_file);
							}
						$output_path = './assets/pdf_digital/hasil/'.$path;
						// print_r($output_path);die();

						
						if(!file_exists ( FCPATH.$path_pdf ))
						{
							array_push($nama_peserta,$value->nama);
							print_r($value->nama);
							die();
						}


						if(!file_exists ( $path_p12 ))
						{ // Cek file p12
							// echo "FIle Sertifikat Tidak ditemukan\n";
							$this->session->set_flashdata('alert','p12 tidak ditemukan');
							redirect('kelas/versi_2','refresh');
						}

						        

						if (!($cert_store = file_get_contents($path_p12))) {
						  //cek baca Sertifikat          
							// echo "File Sertifikat tidak bisa dibaca\n";
							$this->session->set_flashdata('alert','p12 tidak dibaca');
							redirect('kelas/versi_2','refresh');
						        
						}

						        
						if (!openssl_pkcs12_read($cert_store, $cert_info, $passphrase)) {
						  //cek passpharse        
							// echo "Error: Passphrase Salah.\n";
							$this->session->set_flashdata('alert','passphrase salah');
							redirect('peserta/versi_2/'.$id_kelas,'refresh');
						        
						}
						
						
						$JAVA_HOME = "C:\Program Files (x86)\Java\jdk1.7.0_21";
						$PATH = "$JAVA_HOME\bin";
						putenv("JAVA_HOME=$JAVA_HOME");
						putenv("PATH=$PATH");
						
						$tsa_url = 'http://tsa-bsre.bssn.go.id/';
						$ocsp_url = 'http://cvs-osd.lemsaneg.go.id/ocsp';

						$command = 'java -jar "'.$path_jar.'" "'.$path_pdf.'" -kst PKCS12 -ksf "'.$path_p12.'" -ksp "'.$passphrase.'" -tsh SHA256 -ha SHA256 -d "'.$output_path.'" -os "_signed" -llx 564 -lly 96 -urx 765 -ury 49 --bg-scale 0 -V --l4-text "" --l2-text ""  ';

						$command .= '-ts '.$tsa_url.' -ta PASSWORD -tsu "coba" -tsp "1234" --ocsp --ocsp-server-url '.$ocsp_url.' ';
						        
						
						exec($command, $val, $er);
						// print_r($val);die;
						if ($er == 0 || $er == 3) 
						{
							$where = 
							[
								'id_kelas' => $value->id_kelas,
								'noktp' => $value->noktp,
							];
							$data = 
							[
								'id_kelas' => $value->id_kelas,
								'noktp' => $value->noktp,
								'status' => 1,
								'file_pdf_digital' => $file->file_pdf,
								'log_value_bsre' => json_encode($val)
							];
							$cek = $this->db->get_where('lock_ttd_digital',['id_kelas' => $value->id_kelas, 'noktp' => $value->noktp,'status'=>0])->row();
							if(!empty($cek))
							{
								$this->db->update('lock_ttd_digital',$data,$where);
							}
							$this->session->set_flashdata('alert','sukses');
						} 
						else 			// kalau gagal tolong simpan $val di file (log)
						{
								$where = 
								[
									'id_kelas' => $value->id_kelas,
									'noktp' => $value->noktp,
								];
								$data = 
								[
									'log_value_bsre' => $value->noktp.' gagal generate',
								];
								$cek = $this->db->get_where('lock_ttd_digital',['id_kelas' => $value->id_kelas, 'noktp' => $value->noktp,'status'=>0])->row();
								if(!empty($cek))
								{
									$this->db->update('lock_ttd_digital',$data,$where);
								}
							$this->session->set_flashdata('alert','gagal');
						}
				}
				// Code Old //
				// header('Refresh: 1;url='.base_url('Cetak_digital/index/'.$id_kelas.'/'.($offset+1).'/'.$total));
				// print_r($peserta);
			}
			else 
			{
				// echo 'KOSONG';
				// header('Refresh: 1;url='.base_url('Cetak_digital/index/'.$id_kelas.'/'.$offset));
			}
		}
		/////// Tambahan Progres bar ////////


		if ($er == 0 || $er == 3) 
		{
			$data = 
			[
				'id_kelas' => $id_kelas,
				'status' => 3,
				'user' => 'kapus',
				'keterangan' => 'approve'
			];

			$this->db->insert('ttd_digital_history',$data);
			$this->db->update('kelas',['status_approve'=>3],['id_kelas'=>$id_kelas]);	
		}
		// tambahan //
		else
		{
			$this->session->set_flashdata('nama_peserta','gagal');
			redirect('kelas/versi_2','refresh');
		}
		// tambahan //

		$this->load->view('progres_bar', $data);
	}

	// tambahan //
	public function cek_pin()
	{
		$input = $this->input->post();
		$id_kelas = $input['kelas'];
		$pin = $input['passphrase'];
		$this->session->set_userdata('passphrase', $pin);

		redirect("cetak_digital/$id_kelas/0/0");
	}
	// tambahan //
}
