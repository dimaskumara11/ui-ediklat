<!DOCTYPE HTML>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
<title>Approval Certificate </title>
<link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/style.css">
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/framework.css">
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/fonts/css/fontawesome-all.min.css">    
</head>
    
<body class="theme-light"  style="background: #0D2D6C">
    
<div id="page-preloader">  
    <div class="loader-main">
        <div class="preload-spinner preload-large border-highlight"></div>
    </div> 
</div>
<div class="preloader__"></div>
<div class="bg-yellow1-dark" id="reading-progress-bar"></div>
<div id="page" class="halaman">
    <div class="header header-fixed header-logo-app" style="background: #0D2D6C">
        <a href="#" onclick="goBack()" class="header-title" style="color: white">Back</a>
        <a href="#" onclick="goBack()" class="header-icon header-icon-1"><i style="color: white" class="fas fa-arrow-left"></i></a>
        <?php
        if($status_approve==0)
        {
        ?>
            <a href="#" class="header-icon header-icon-4" data-menu="menu-forgot">
                <button class="button button-full button-m shadow-large button-round-small top-10 bottom-0" style="cursor:pointer; background: yellow; color: blue">Approve</button>
            </a>
        <?php
        }?>
    </div>
        
    <div id="menu-1" class="menu menu-box-left" 
         data-menu-width="250"
         data-menu-effect="menu-over"
         data-menu-select="page-components">
         <?php $this->load->view('menu'); ?>
    </div>  
         

    <div class="page-content header-clear-medium">   
    <?php
    if($this->session->flashdata('alert') == "passphrase salah")
    {?>
        <div class="alert alert-large alert-round-medium bg-red2-dark alert_flash">
            <i class="fa fa-times-circle"></i>
            <strong class="uppercase ultrabold">Gagal, <br>Pin Yang Dimasukkan salah</strong>
            <span></span>
            <i class="fa fa-times"></i>
        </div>  
    <?php
    }
    else if($this->session->flashdata('alert') == "Password Salah")
    {?>
        <div class="alert alert-large alert-round-medium bg-red2-dark alert_flash">
            <i class="fa fa-times-circle"></i>
            <strong class="uppercase ultrabold">Sukses, <br>Password Salah</strong>
            <span></span>
            <i class="fa fa-times"></i>
        </div>  
    <?php
    }
    ?>
                <div data-height="200" class="caption caption-margins round-medium shadow-large">
                    <div class="caption-center">
                        <h1 class="center-text color-white font-16"> <?php 
                                ($q_kelas["id_diklat_versi"] > 0) ? $ver ='<span class="badge badge-danger">VERSI</span> ' : $ver = '';
                                echo $ver.$q_kelas["nama_diklat"];
                            ?></h1>
                        <p class="boxed-text-large color-white opacity-80 bottom-20">
                            <?php echo date_indo($q_kelas["tgl_mulai"]).' s.d '.date_indo($q_kelas["tgl_akhir"])?>
                        </p>
                        <p class="boxed-text-large color-white opacity-80 bottom-0">
                            <?php
                                $wi = $this->db->select('nama')->join('wi_pengajar','wi_pengajar.id_pengajar = kelas_wi.id_wi')->get_where('kelas_wi',['id_kelas'=>$q_peserta[0]['id_kelas']])->result();
                                // echo $wi->nama;
                                foreach ($wi as $key => $value) 
                                {
                                    echo $value->nama.'<br>';
                                }
                            ?>
                        </p>
                    </div>
                    <div class="caption-overlay bg-black opacity-90"></div>
                    <div class="caption-bg bg-13"></div>
                </div> 

            <?php
            $no = 1;
            foreach($q_peserta as $dt)
            {
                $cek_status_generate = $this->db->select('status,file_pdf_digital,count(id) as total')->get_where('lock_ttd_digital',['id_kelas'=>$dt['id_kelas'],'noktp'=>$dt['noktp']])->row();
                $nilai['nilai_akhir_1'] = '92.5|100'; // sangat memuaskan
                $nilai['nilai_akhir_2'] = '85|92.4'; // memuaskan
                $nilai['nilai_akhir_3'] = '77.5|84.9'; // baik sekali
                $nilai['nilai_akhir_4'] = '70|77.4'; // baik
                $nilai['nilai_akhir_5'] = '70'; // tidak lulus
                $nilai_akhir_1 = explode('|', $nilai["nilai_akhir_1"]);
                $nilai_akhir_2 = explode('|', $nilai["nilai_akhir_2"]);
                $nilai_akhir_3 = explode('|', $nilai["nilai_akhir_3"]);
                $nilai_akhir_4 = explode('|', $nilai["nilai_akhir_4"]);
                $nilai_akhir_5 = $nilai["nilai_akhir_5"];
                if($dt["nilai_akhir"] < $nilai_akhir_5)
                    $predikat = 'Tidak Lulus';
                elseif($dt["nilai_akhir"] >= $nilai_akhir_4[0] AND $dt["nilai_akhir"] <= $nilai_akhir_4[1])
                    $predikat = 'Baik';
                elseif($dt["nilai_akhir"] >= $nilai_akhir_3[0] AND $dt["nilai_akhir"] <= $nilai_akhir_3[1])
                    $predikat = 'Baik Sekali';
                elseif($dt["nilai_akhir"] >= $nilai_akhir_2[0] AND $dt["nilai_akhir"] <= $nilai_akhir_2[1])
                    $predikat = 'Memuaskan';
                elseif($dt["nilai_akhir"] >= $nilai_akhir_1[0] AND $dt["nilai_akhir"] <= $nilai_akhir_1[1])
                    $predikat = 'Sangat Memuaskan';
                switch($dt["status_lulus"]){
                    case '0':
                        $status = 'Pending';
                        break;
                    case '1':
                        $status = 'Lulus';
                        break;
                    case '2':
                        $status = 'Tidak Lulus';
                        break;
                    case '3':
                        $status = 'Batal';
                        break;
                    default:
                        $status = '-';
                        break;
                }
            ?>   
                <div class="link-list link-list-3">
            <?php
                if($cek_status_generate->status == 1)
                {
            ?>
                    <a href="#" class="round-small shadow-tiny lihat_sert" link="<?=base_url()?>assets/pdf_digital/<?=$cek_status_generate->file_pdf_digital?>" style="padding: 5px 0px 5px 0px !important;margin-bottom: 5px !important;">
            <?php
                }
                else
                {
            ?>
                    <a href="#" class="round-small shadow-tiny lihat_sert" link="<?=base_url()?>assets/pdf_digital/<?=$cek_status_generate->file_pdf_digital?>" style="padding: 5px 0px 5px 0px !important;margin-bottom: 5px !important;">
            <?php }?>
                        <i class="font-15" style="color: black !important; font-style: normal; margin-top: -13px !important;width: 20px"><?=$no++?>.</i>
                        <span style="padding-left: 35px;"><?=ucwords(strtolower($dt["nama"]))?></span>
                        <strong style="padding-left: 35px;margin-top: 1px !important; font-size: 12px; color: black"><?=$dt["noktp"]?></strong>
                        <em style="right: 45px;margin-top: -43px !important;width: 120px;text-align: center;" class="font-16"><?=$dt['nilai_akhir']?></em>
                        <em style="right: 45px;margin-top: -22px !important; font-size: 9px !important;width: 120px;text-align: center"><?=$predikat?></em>
                        <?php
                            if($cek_status_generate->status == 1)
                            {
                                echo '<em class="color-green1-dark">Berhasil</em>';
                                echo '<i class="fa fa-check-circle color-green1-dark"></i>';
                            }
                            else
                            {
                                echo '<em class="color-red1-dark">Belum di TTD</em>';
                                echo '<i class="fa fa-circle color-red1-dark"></i>';
                            }
                            ?>
                    </a>
                </div>      
            <?php
            }
            ?> 
        </div>
    </div>
    
    <div id="menu-forgot" 
         class="menu menu-box-modal round-medium halaman" 
         data-menu-height="250" 
         data-menu-width="510" 
         data-menu-effect="menu-over">
        <div class="content">
            <div class="akses" id="<?=$this->session->userdata('hak_akses');?>"></div>
                <?php
                if($this->session->userdata('hak_akses')==1)
                {
                ?>
                    <form action="<?=base_url()?>Approve/" method="post">
                <?php
                }
                elseif ($this->session->userdata('hak_akses')==2)
                {
                ?>
                    <form action="<?=base_url()?>Cetak_digital/" method="post">
                <?php
                }
                ?>
                <h2 class="center-text uppercase ultrabold top-30">Forgot Password?</h2>
                <p class="center-text font-24 under-heading bottom-20" style="font-weight: bold">
                    Masukkan PIN Anda
                </p>
                    <input type="hidden" name="kelas" value="<?=$q_peserta[0]['id_kelas']?>">
                <div class="input-style has-icon input-style-2 input-required bottom-30">
                    <i class="input-icon fa fa-at"></i>
                    <span>PIN</span>
                    <em>(Harap dimasukkan)</em>
                    <input type="password" name="passphrase" placeholder="PIN" required="">
                </div>   
                <button id="approve_" style="width: 100%" class="button button-l shadow-large button-round-small bg-highlight">APPROVE</button>
            </form>
        </div>
    </div>
    <div style="border-top:solid; border-color: #d09c0a; background: #0D2D6C; height: 40px"></div>
    <div class="footer">
        <a href="#" class="footer-title"><span class="color-highlight">e </span>PELATIHAN</a>
        <p class="footer-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. </p>
        <div class="footer-socials">
            <a href="#" class="round-tiny shadow-medium bg-facebook"><i class="fab fa-facebook-f"></i></a>
            <a href="#" class="round-tiny shadow-medium bg-twitter"><i class="fab fa-twitter"></i></a>
            <a href="#" class="round-tiny shadow-medium bg-phone"><i class="fa fa-phone"></i></a>
            <a href="#" data-menu="menu-share" class="round-tiny shadow-medium bg-red2-dark"><i class="fa fa-share-alt"></i></a>
            <a href="#" class="back-to-top round-tiny shadow-medium bg-dark1-light"><i class="fa fa-angle-up"></i></a>
        </div>
        <p class="footer-copyright">Copyright &copy; BPSDM PUPR <span id="copyright-year">2019</span>. All Rights Reserved.</p>
    </div>  

</div>

<script type="text/javascript" src="<?=base_url()?>assets/scripts/jquery.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/scripts/plugins.js" async></script>
<script type="text/javascript" src="<?=base_url()?>assets/scripts/custom.js" async></script>
<script type="text/javascript">
    setTimeout(function()
    { 
        $('.alert_flash').fadeOut('slow');
    }, 3000);
    $(document).ready(function()
    {
        $('#approve_').on('click',function()
        {
            $('.halaman').hide();
            var akses = $('.akses').attr('id');
            if(akses == 1)
            {
                $('.preloader__').html(`
                <div class="demo-preloader" style="margin:25% 15% 25% 35%;">
                    <div class="preload-spinner preload-large border-highlight"></div>
                    <br><br><br>
                    <h1 align="center"><b>Mohon Tunggu,<br> Proses Generate Sedang Berlangsung</b></h1>
                </div>
                `)
                $('body').click();
            }
            else if(akses == 2)
            {
                $('.preloader__').html(`
                <div class="demo-preloader" style="margin:25% 15% 25% 35%;">
                    <div class="preload-spinner preload-large border-highlight"></div>
                    <br><br><br>
                    <h1 align="center"><b>Mohon Tunggu,<br> Proses Generate Tanda Tangan Digital Sedang Berlangsung</b></h1>
                </div>
                `)
                $('body').click();
            }
        })
    })
</script>
<script>
function goBack() {
  window.history.back();
}

$(document).ready(function()
{
    $('.lihat_sert').on('click',function()
    {
        var url = $(this).attr('link');
        window.open(url, '_blank')
    })
})
</script>
</body>
