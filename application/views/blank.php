<!DOCTYPE HTML>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
<title>Approval Certificate </title>
<link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/style.css">
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/framework.css">
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/fonts/css/fontawesome-all.min.css">    
</head>
    
<body class="theme-light bg-blue1-light" data-highlight="blue2">
    
<div id="page-preloader">
    <div class="loader-main"><div class="preload-spinner border-highlight"></div></div>
</div>

    
<div id="page">
    
</div>


<script type="text/javascript" src="<?=base_url()?>assets/scripts/jquery.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/scripts/plugins.js" async></script>
<script type="text/javascript" src="<?=base_url()?>assets/scripts/custom.js" async></script>
<script type="text/javascript">
    setTimeout(function()
    { 
        $('.alert_flash').fadeOut('slow');
    }, 3000);
</script>
</body>
