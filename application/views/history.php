<!DOCTYPE HTML>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
<title>Approval Certificate </title>
<link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/style.css">
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/framework.css">
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/fonts/css/fontawesome-all.min.css">    
</head>
    
<body class="theme-light"  style="background: #0D2D6C">
    
<div id="page-preloader">  
    <div class="loader-main">
        <div class="preload-spinner preload-large border-highlight"></div>
    </div> 
</div>
<div class="preloader__"></div>
<div class="bg-yellow1-dark" id="reading-progress-bar"></div>
<div id="page" class="halaman">
    <div class="header header-fixed header-logo-app" style="background: #0D2D6C">
        <a href="#" style="color: white" class="header-title ultrabold"><span style="color: yellow">APPROVE</span> SERTIFICATE</a>
    </div>
        
    <div id="menu-1" class="menu menu-box-left" 
         data-menu-width="250"
         data-menu-effect="menu-over"
         data-menu-select="page-components">
         <?php $this->load->view('menu'); ?>
    </div>  
         

    <div class="page-content header-clear-medium">   
        <div data-height="100" class="caption caption-margins round-medium shadow-large">
            <div class="caption-center">
                <h1 class="center-text color-white font-16">History Persetujuan Dan Penolakan</h1>
            </div>
            <div class="caption-overlay bg-black opacity-90"></div>
            <div class="caption-bg bg-13"></div>
        </div> 

            <?php
            $no = 1;
            foreach($history as $hs)
            {
            ?>
                <div class="link-list link-list-3">
                    <a href="<?=base_url()?>?>" class="round-small shadow-tiny " style="padding: 5px 0px 5px 0px !important;margin-bottom: 5px !important;" target="_blank">
                        <i class="font-14" style="color: black !important; font-style: normal; margin-top: -24px !important;width: 20px"><?=$no++?>.</i>
                        <span style="padding-left: 35px;"><?=$hs->keterangan?></span>
                        <strong style="padding-left: 35px;margin-top: 1px !important; font-size: 12px; color: black">By : <?=$hs->username?></strong>
                        <strong style="padding-left: 35px;margin-top: 1px !important; font-size: 12px; color: black">Alasan : <?=(isset($hs->alasan)) ? $hs->alasan : '-';?></strong>
                        <em style="right: 0;margin-top: -40px !important;width: 95px;text-align: center;"><i class="fa fa-clock font-23"></i></em>
                        <em style="right: 0;margin-top: -22px !important; font-size: 11px !important;width: 120px;text-align: center"><?=$hs->created?></em>
                    </a>
                </div>      
            <?php
            }
            ?> 
        </div>

    <div style="border-top:solid; border-color: #d09c0a; background: #0D2D6C; height: 40px"></div>
    <div id="footer-menu" class="footer-menu-3-icons">
        <!-- <a href="pages-list.html"><i class="fa fa-heart"></i><span>Pages</span></a> -->
        <a href="<?=base_url()?>Home" class="active-menu"><i class="fa fa-home"></i><span>Home</span></a>
        <a href="<?=base_url()?>Kelas/versi_2"><i class="fa fa-edit"></i><span>Kelas</span></a>
        <a href="<?=base_url()?>Kelas/berada_di_kapus"><i class="fa fa-exclamation-triangle"></i><span>Berada Di Kapus</span></a>
        <!-- <a href="page-contact.html"><i class="fa fa-envelope"></i><span>Contact</span></a> -->
        <div class="clear"></div>
    </div>
    <div class="footer">
        <a href="#" class="footer-title"><span class="color-highlight">e </span>PELATIHAN</a>
        <p class="footer-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. </p>
        <div class="footer-socials">
            <a href="#" class="round-tiny shadow-medium bg-facebook"><i class="fab fa-facebook-f"></i></a>
            <a href="#" class="round-tiny shadow-medium bg-twitter"><i class="fab fa-twitter"></i></a>
            <a href="#" class="round-tiny shadow-medium bg-phone"><i class="fa fa-phone"></i></a>
            <a href="#" data-menu="menu-share" class="round-tiny shadow-medium bg-red2-dark"><i class="fa fa-share-alt"></i></a>
            <a href="#" class="back-to-top round-tiny shadow-medium bg-dark1-light"><i class="fa fa-angle-up"></i></a>
        </div>
        <p class="footer-copyright">Copyright &copy; BPSDM PUPR <span id="copyright-year">2019</span>. All Rights Reserved.</p>
    </div>  
</div>

<script type="text/javascript" src="<?=base_url()?>assets/scripts/jquery.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/scripts/plugins.js" async></script>
<script type="text/javascript" src="<?=base_url()?>assets/scripts/custom.js" async></script>
<script type="text/javascript">
    setTimeout(function()
    { 
        $('.alert_flash').fadeOut('slow');
    }, 3000);
    $(document).ready(function()
    {
        $('#approve_').on('click',function()
        {
            $('.halaman').hide();
            var akses = $('.akses').attr('id');
            if(akses == 1)
            {
                $('.preloader__').html(`
                <div class="demo-preloader" style="margin:25% 15% 25% 35%;">
                    <div class="preload-spinner preload-large border-highlight" style="color:#d09c0a"></div>
                    <br><br><br>
                    <h1 align="center" style="color:#d09c0a"><b>Mohon Tunggu,<br> Proses Generate Sedang Berlangsung</b></h1>
                </div>
                `)
                $('.tutup_modal_approve').click();
                $('.footer').hide();
            }
            else if(akses == 2)
            {
                $('.preloader__').html(`
                <div class="demo-preloader" style="margin:25% 15% 25% 35%;">
                    <div class="preload-spinner preload-large border-highlight" style="color:#d09c0a"></div>
                    <br><br><br>
                    <h1 align="center" style="color:#d09c0a"><b>Mohon Tunggu,<br> Proses Generate Tanda Tangan Digital Sedang Berlangsung</b></h1>
                </div>
                `)
                $('.tutup_modal_approve').click();
                $('.footer').hide();
            }
        })
        $('.tutup_modal_approve').on('click',function()
        {
            $('#menu-forgot').hide('fast');
        })
        $('.setuju').on('click',function()
        {
            $('#menu-forgot').show('fast');
        })

        $('.tolak').on('click',function()
        {
            $('#menu-tolak').show('fast');
        })
        $('.tutup_modal_tolak').on('click',function()
        {
            $('#menu-tolak').fadeOut('fast');
        })
    })
</script>
<script>
function goBack() {
  window.history.back();
}

</script>
</body>