<!DOCTYPE HTML>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
<title>Approval Certificate </title>
<link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/style.css">
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/framework.css">
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/fonts/css/fontawesome-all.min.css">    
</head>
    
<body class="theme-light">
    
<div id="page-preloader">
    <div class="loader-main"><div class="preload-spinner border-highlight"></div></div>
</div>

<div id="footer-menu" class="footer-menu-3-icons">
    <!-- <a href="pages-list.html"><i class="fa fa-heart"></i><span>Pages</span></a> -->
    <a href="<?=base_url()?>Home" class="active-menu"><i class="fa fa-home"></i><span>Home</span></a>
    <a href="<?=base_url()?>Kelas/versi_2"><i class="fa fa-edit"></i><span>Kelas</span></a>
    <a href="media.html"><i class="fa fa-exclamation-triangle"></i><span>Berada Di Kapus</span></a>
    <!-- <a href="page-contact.html"><i class="fa fa-envelope"></i><span>Contact</span></a> -->
    <div class="clear"></div>
</div>
    
<div id="page">
	<div class="header header-fixed header-logo-app" style="background: #0D2D6C">
        <a href="#" style="color: white" class="header-title ultrabold"><span style="color: yellow">APPROVE</span> SERTIFICATE</a>
        <?php
        if($this->session->userdata('hak_akses')==1)
        {
        ?>
            <a href="<?=base_url()?>Logout" class="header-icon header-icon-3" style="font-weight: bold;font-size: 14px;color: white">Keluar</a>
        <?php
        }
        ?>
	</div>
        
    <div id="menu-1" class="menu menu-box-left" 
         data-menu-width="250"
         data-menu-effect="menu-over"
         data-menu-select="page-components">
         <?php $this->load->view('menu'); ?>
    </div>    
         

    <div class="page-content header-clear-medium">	 
            <?php
            if($this->session->flashdata('alert') == "sukses")
            {
            ?>
                <div class="alert alert-large alert-round-medium bg-green1-dark alert_flash">
                    <i class="fa fa-check"></i>
                    <strong class="uppercase ultrabold">Sukses, <br>Generate Digital Sertifikat Berhasil</strong>
                    <span></span>
                    <i class="fa fa-times"></i>
                </div> 
            <?php
            } 
            // else
            else if($this->session->flashdata('alert') == "gagal")
            {?>
                <div class="alert alert-large alert-round-medium bg-red2-dark alert_flash">
                    <i class="fa fa-times-circle"></i>
                    <strong class="uppercase ultrabold">Gagal, <br>Generate Digital Sertifikat Gagal</strong>
                    <span></span>
                    <i class="fa fa-times"></i>
                </div>  
            <?php
            }
            else if($this->session->flashdata('alert') == "koneksi")
            {?>
                <div class="alert alert-large alert-round-medium bg-red2-dark alert_flash">
                    <i class="fa fa-times-circle"></i>
                    <strong class="uppercase ultrabold">Gagal, <br>Tolong Cek Koneksi Internet Anda</strong>
                    <span></span>
                    <i class="fa fa-times"></i>
                </div>  
            <?php
            }
            else if($this->session->flashdata('alert') == "p12 tidak ditemukan")
            {?>
                <div class="alert alert-large alert-round-medium bg-red2-dark alert_flash">
                    <i class="fa fa-times-circle"></i>
                    <strong class="uppercase ultrabold">Gagal, <br>File p12 Tidak Di Temukan</strong>
                    <span></span>
                    <i class="fa fa-times"></i>
                </div>  
            <?php
            }
            else if($this->session->flashdata('alert') == "p12 tidak dibaca")
            {?>
                <div class="alert alert-large alert-round-medium bg-red2-dark alert_flash">
                    <i class="fa fa-times-circle"></i>
                    <strong class="uppercase ultrabold">Gagal, <br>File p12 Tidak Terbaca</strong>
                    <span></span>
                    <i class="fa fa-times"></i>
                </div>  
            <?php
            }
            else if($this->session->flashdata('alert') == "passphrase salah")
            {?>
                <div class="alert alert-large alert-round-medium bg-red2-dark alert_flash">
                    <i class="fa fa-times-circle"></i>
                    <strong class="uppercase ultrabold">Gagal, <br>Pin Yang Dimasukkan salah</strong>
                    <span></span>
                    <i class="fa fa-times"></i>
                </div>  
            <?php
            }
            else if($this->session->flashdata('alert') == "Approve Berhasil")
            {?>
                <div class="alert alert-large alert-round-medium bg-green1-dark alert_flash">
                    <i class="fa fa-check"></i>
                    <strong class="uppercase ultrabold">Sukses, <br>Approve Berhasil</strong>
                    <span></span>
                    <i class="fa fa-times"></i>
                </div>  
            <?php
            }
            else if($this->session->flashdata('alert') == "ada kesalahan")
            {?>
                <div class="alert alert-large alert-round-medium bg-red2-dark alert_flash">
                    <i class="fa fa-check"></i>
                    <strong class="uppercase ultrabold">Error, <br>Ada Kesalahan</strong>
                    <span></span>
                    <i class="fa fa-times"></i>
                </div>  
            <?php
            }
            else if($this->session->flashdata('alert') == "penolakan berhasil")
            {?>
                <div class="alert alert-large alert-round-medium bg-green1-dark alert_flash">
                    <i class="fa fa-check"></i>
                    <strong class="uppercase ultrabold">Sukses, <br>Penolakan Berhasil</strong>
                    <span></span>
                    <i class="fa fa-times"></i>
                </div>  
            <?php
            }
                                
            $no = 1;
            foreach ($q_kelas as $dt)   
            {
                $cek_status_generate = $this->db->select('status')->get_where('lock_ttd_digital',['id_kelas'=>$dt['id_kelas']])->row();
                // print_r($cek_status_generate);die();

                ($dt["id_diklat_versi"] > 0) ? $ver ='<span class="badge badge-danger">VERSI</span> ' : $ver = '';
            ?>

        
                <div data-height="200" class="caption caption-margins round-medium shadow-large">
                    <div class="caption-center">
                        <h1 class="center-text color-white font-16"><?=$dt["nama_diklat"]?></h1>
                        <p class="boxed-text-large color-white opacity-80 bottom-30">
                            <?=$dt["balai_alias"]?>, <?=date_indo($dt["tgl_mulai"]) .' s.d '. date_indo($dt["tgl_akhir"]) . ' Kelas '.$dt["kelas"];?>
                        </p>
                        <a href="<?=site_url()?>peserta_/versi_2/<?=$dt["id_kelas"]?>" class="button button-xs button-round-medium button-center bottom-0" style="background: yellow; color: blue">Lihat Peserta</a>
                    </div>
                    <div class="caption-overlay bg-black opacity-90"></div>
                    <div class="caption-bg bg-13"></div>
                </div> 
                <?php
            }
            ?>
    <div style="border-top:solid; border-color: #d09c0a; background: #0D2D6C; height: 40px"></div>
    <div class="footer">
        <a href="#" class="footer-title"><span class="color-highlight">e </span>PELATIHAN</a>
        <p class="footer-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. </p>
        <div class="footer-socials">
            <a href="#" class="round-tiny shadow-medium bg-facebook"><i class="fab fa-facebook-f"></i></a>
            <a href="#" class="round-tiny shadow-medium bg-twitter"><i class="fab fa-twitter"></i></a>
            <a href="#" class="round-tiny shadow-medium bg-phone"><i class="fa fa-phone"></i></a>
            <a href="#" data-menu="menu-share" class="round-tiny shadow-medium bg-red2-dark"><i class="fa fa-share-alt"></i></a>
            <a href="#" class="back-to-top round-tiny shadow-medium bg-dark1-light"><i class="fa fa-angle-up"></i></a>
        </div>
        <p class="footer-copyright">Copyright &copy; BPSDM PUPR <span id="copyright-year">2019</span>. All Rights Reserved.</p>
    </div> 
                   
    </div>   
</div>

<script type="text/javascript" src="<?=base_url()?>assets/scripts/jquery.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/scripts/plugins.js" async></script>
<script type="text/javascript" src="<?=base_url()?>assets/scripts/custom.js" async></script>
</body>
</html>