<?php
	if (!defined('gampil')) die("gampil - Invalid Route");
	
	$CONFIG['db']['use'] = true;
	$CONFIG['db']['host'] = 'localhost';
	$CONFIG['db']['name'] = 'pelatihan-beta';
	$CONFIG['db']['user'] = 'pelatihan';
	$CONFIG['db']['pass'] = 'pelatihan1234!';

	$CONFIG['site']['theme'] = 'theme';
	$CONFIG['site']['url'] = 'https://bpsdm.pu.go.id/center/pelatihan-beta/'; // JANGAN LUPA TANDA SLASH DIBELAKANG

	$URL['assets_url'] = 'https://bpsdm.pu.go.id/center/pelatihan/view/theme/'; // referensi ke link
	
	$CONFIG['captcha'] = false;
	$CONFIG['recaptcha']['sitekey'] = '';
	$CONFIG['recaptcha']['secretkey'] = ''; 

	$CONFIG['debug'] = false;

	$CONFIG['maintenance'] = false;
	$CONFIG['maintenancetext'] = "Saat ini sedang dilakukan perawatan server berkala.";

	$CONFIG['max_pelatihan'] = 60; // jumlah maksimal per balai DIPA
	$CONFIG['max_peserta'] = 5; // jumlah maksimal per pelatihan untuk peserta

	$CONFIG['materi'] = 70;
	$CONFIG['k3'] = 0;
	$CONFIG['seminar'] = 0;
	$CONFIG['sikap'] = 30;
	
	$CONFIG['nilai_akhir_1'] = '92.5|100'; // sangat memuaskan
	$CONFIG['nilai_akhir_2'] = '85|92.4'; // memuaskan
	$CONFIG['nilai_akhir_3'] = '77.5|84.9'; // baik sekali
	$CONFIG['nilai_akhir_4'] = '70|77.4'; // baik
	$CONFIG['nilai_akhir_5'] = '70'; // tidak lulus
	
	$CONFIG['sertifikat_lokasi'] = 'Jakarta';
	$CONFIG['sertifikat_kepala'] = "Badan Pengembangan Sumber Daya Manusia";
	$CONFIG['sertifikat_nama'] = "Ir. Lolly Martina Martief, M.T";
	$CONFIG['sertifikat_nip'] = "196001101988032001";
	
	$CONFIG['sertifikat_lokasi_lan'] = 'Jakarta';
	$CONFIG['sertifikat_kepala_lan'] = "Deputi Bidang Diklat Aparatur Lembaga Administrasi Negara";
	$CONFIG['sertifikat_nama_lan'] = "Dr. Muhammad Idris, MSi";
	$CONFIG['sertifikat_nip_lan'] = "196411151993031001";
		
	$CONFIG['sertifikat_lokasi_lan_kapus'] = 'Jakarta';
	$CONFIG['sertifikat_kepala_lan_kapus'] = "Kepala Pusat Pengembangan Program dan Pembinaan Diklat Deputi Bid. Diklat Aparatur - LAN RI";
	$CONFIG['sertifikat_nama_lan_kapus'] = "Erfi Muthmainah, SS., MA";
	$CONFIG['sertifikat_nip_lan_kapus'] = "197102091995032005";
		
	$CONFIG['sertifikat_lokasi_kapus'] = 'Jakarta';
	$CONFIG['sertifikat_kepala_kapus'] = "Pusat Diklat Manajemen dan Pengembangan Jabatan Fungsional";
	$CONFIG['sertifikat_nama_kapus'] = "Ir. Nicodemus Daud, M.Si";
	$CONFIG['sertifikat_nip_kapus'] = "196412301997031002";
	
	//email kofig
	$CONFIG['e_email'] = 'indrajatmiko@gmail.com';
	$CONFIG['e_pass'] = '';
	$CONFIG['e_from'] = 'e-Pelatihan BPSDM PUPR';
	
	//////////////////////////////////////////////////////////////////////////////////////
	///// ROUTING - MULAI
	$PAGES = array();
	
	$PAGES['login']['module'] = 'login, table, ui, query';
	$PAGES['login']['control'] = 'login';
	$PAGES['login_cek']['module'] = 'login,table, ui';
	$PAGES['login_cek']['control'] = 'login_cek';

	$PAGES['logout']['module'] = 'login, ui';
	$PAGES['logout']['control'] = 'logout';

	$PAGES['beranda']['module'] = 'table, ui, html2text, login, query';
	$PAGES['beranda']['control'] = 'dashboard';

	$PAGES['gen']['module'] = 'generator';
	$PAGES['gen']['control'] = 'gen_kelas';
	
	$PAGES['gen_info']['module'] = 'generator';
	$PAGES['gen_info']['control'] = 'gen_info';
	
	$PAGES['tmp']['module'] = 'table, ui, html2text, login, query';
	$PAGES['tmp']['control'] = 'tmp';
	
	$PAGES['master_balai']['module'] = 'table, ui, html2text, login, query';
	$PAGES['master_balai']['control'] = 'master/master_balai';	
	$PAGES['profil_balai']['module'] = 'table, ui, html2text, login, query';
	$PAGES['profil_balai']['control'] = 'master/profil_balai';
	$PAGES['sarana_balai']['module'] = 'table, ui, html2text, login, query';
	$PAGES['sarana_balai']['control'] = 'master/sarana_balai';
	
	$PAGES['jenis_diklat']['module'] = 'table, ui, html2text, login, query';
	$PAGES['jenis_diklat']['control'] = 'diklat/jenis_diklat';
	$PAGES['bidang_pelatihan']['module'] = 'table, ui, html2text, login, query';
	$PAGES['bidang_pelatihan']['control'] = 'diklat/bidang_pelatihan';
	$PAGES['bidang_pekerjaan']['module'] = 'table, ui, html2text, login, query';
	$PAGES['bidang_pekerjaan']['control'] = 'diklat/bidang_pekerjaan';
	$PAGES['jenis_kegiatan']['module'] = 'table, ui, html2text, login, query';
	$PAGES['jenis_kegiatan']['control'] = 'diklat/jenis_kegiatan';
	$PAGES['model_pelatihan']['module'] = 'table, ui, html2text, login, query';
	$PAGES['model_pelatihan']['control'] = 'diklat/model_pelatihan';
	$PAGES['modul_diklat']['module'] = 'table, ui, html2text, login, query';
	$PAGES['modul_diklat']['control'] = 'diklat/modul_diklat';
	$PAGES['diklat']['module'] = 'table, ui, html2text, login, query';
	$PAGES['diklat']['control'] = 'diklat/diklat';
	$PAGES['diklat-versi']['module'] = 'table, ui, html2text, login, query';
	$PAGES['diklat-versi']['control'] = 'diklat/diklat_versi';
	$PAGES['diklat-versi-detail']['module'] = 'table, ui, html2text, login, query';
	$PAGES['diklat-versi-detail']['control'] = 'diklat/diklat_versi_detail';
	$PAGES['diklat-detail']['module'] = 'table, ui, html2text, login, query';
	$PAGES['diklat-detail']['control'] = 'diklat/diklat_detail';
	
	$PAGES['ajax_diklat']['module'] = 'table, ui, login, query';
	$PAGES['ajax_diklat']['control'] = 'diklat/ajax_diklat';
	$PAGES['ajax_modul']['module'] = 'table, ui, login, query';
	$PAGES['ajax_modul']['control'] = 'diklat/ajax_modul';
	$PAGES['typehead-modul']['module'] = 'table, ui, login, query';
	$PAGES['typehead-modul']['control'] = 'diklat/typehead_modul';
	
	$PAGES['bidang_instruktur']['module'] = 'table, ui, html2text, login, query';
	$PAGES['bidang_instruktur']['control'] = 'wi/bidang_Widyaiswara';
	$PAGES['pilih_instruktur']['module'] = 'table, ui, html2text, login, query';
	$PAGES['pilih_instruktur']['control'] = 'wi/bidang_Widyaiswara';
	$PAGES['pengajar']['module'] = 'table, ui, html2text, login, query';
	$PAGES['pengajar']['control'] = 'wi/pengajar';
	$PAGES['ajax-wi']['module'] = 'table, ui, login, query';
	$PAGES['ajax-wi']['control'] = 'wi/ajax_wi';
	$PAGES['usulan-pengajar']['module'] = 'table, ui, login, query';
	$PAGES['usulan-pengajar']['control'] = 'wi/usulan_pengajar';
	
	//soal
	$PAGES['ajax_view_soal']['module'] = 'table, ui, html2text, login, query';
	$PAGES['ajax_view_soal']['control'] = 'soal/ajax_view_soal';
	$PAGES['ajax_view_soal_versi']['module'] = 'table, ui, html2text, login, query';
	$PAGES['ajax_view_soal_versi']['control'] = 'soal/ajax_view_soal_versi';
	$PAGES['add-soal']['module'] = 'table, ui, login, query';
	$PAGES['add-soal']['control'] = 'soal/add_soal';
	$PAGES['add-soal-versi']['module'] = 'table, ui, login, query';
	$PAGES['add-soal-versi']['control'] = 'soal/add_soal_versi';
	$PAGES['bank-soal']['module'] = 'table, ui, html2text, login, query';
	$PAGES['bank-soal']['control'] = 'soal/bank_soal';
	$PAGES['get-soal']['module'] = 'table, ui, html2text, login, query';
	$PAGES['get-soal']['control'] = 'soal/bank_soal';
	$PAGES['get-soal-versi']['module'] = 'table, ui, html2text, login, query';
	$PAGES['get-soal-versi']['control'] = 'soal/bank_soal_versi';
	$PAGES['ajax-soal-modul']['module'] = 'table, ui, login, query';
	$PAGES['ajax-soal-modul']['control'] = 'soal/ajax_modul';
	$PAGES['ajax-soal-modul-detail']['module'] = 'table, ui, login, query';
	$PAGES['ajax-soal-modul-detail']['control'] = 'soal/ajax_modul_soal';
	
	//edoc
	$PAGES['add-file']['module'] = 'table, ui, login, query';
	$PAGES['add-file']['control'] = 'edoc/add_file';
	$PAGES['add-file-versi']['module'] = 'table, ui, login, query';
	$PAGES['add-file-versi']['control'] = 'edoc/add_file_versi';
	$PAGES['e-doc']['module'] = 'table, ui, login, query';
	$PAGES['e-doc']['control'] = 'edoc/e_doc';
	$PAGES['get-e-doc']['module'] = 'table, ui, login, query';
	$PAGES['get-e-doc']['control'] = 'edoc/e_doc';

	//kelas
	$PAGES['kelas']['module'] = 'table, ui, html2text, login, query';
	$PAGES['kelas']['control'] = 'kelas/kelas';	
	$PAGES['ajax-cek-kelas']['module'] = 'table, ui, html2text, login, query';
	$PAGES['ajax-cek-kelas']['control'] = 'kelas/ajax_cek_kelas';
	$PAGES['kelas-kalender']['module'] = 'table, ui, html2text, login, query';
	$PAGES['kelas-kalender']['control'] = 'kelas/kelas_kalender';	
	$PAGES['kelas-kalender-wi']['module'] = 'table, ui, html2text, login, query';
	$PAGES['kelas-kalender-wi']['control'] = 'kelas/kelas_kalender_wi';	
	$PAGES['kelas-instruktur']['module'] = 'table, ui, html2text, login, query';
	$PAGES['kelas-instruktur']['control'] = 'kelas/kelas_wi';	
	$PAGES['ajax-kelas-wi']['module'] = 'table, ui, html2text, login, query';
	$PAGES['ajax-kelas-wi']['control'] = 'kelas/ajax_kelas_wi';	
	$PAGES['ajax-filter-wi']['module'] = 'table, ui, html2text, login, query';
	$PAGES['ajax-filter-wi']['control'] = 'kelas/ajax_filter_wi';	
	$PAGES['kelas-upload']['module'] = 'table, PHPMailerAutoload, ui, html2text, login, query';
	$PAGES['kelas-upload']['control'] = 'kelas/add_file';
	$PAGES['ajax-kelas-evaluasi']['module'] = 'table, ui, html2text, login, query';
	$PAGES['ajax-kelas-evaluasi']['control'] = 'kelas/ajax_evaluasi';	
	$PAGES['excel_export']['module'] = 'table, ui, login, PHPExcel, query';
	$PAGES['excel_export']['control'] = 'kelas/excel_export';
	$PAGES['excel_import']['module'] = 'table, ui, login, PHPExcel, query';
	$PAGES['excel_import']['control'] = 'kelas/excel_import';
	
	$PAGES['sertifikat_lan']['module'] = 'table, ui, login, query';
	$PAGES['sertifikat_lan']['control'] = 'kelas/sertifikat_lan';
	$PAGES['sertifikat_teknis']['module'] = 'table, ui, login, query';
	$PAGES['sertifikat_teknis']['control'] = 'kelas/sertifikat_teknis';

	//peserta
	$PAGES['tambah-peserta']['module'] = 'table, PHPMailerAutoload, ui, html2text, login, query';
	$PAGES['tambah-peserta']['control'] = 'peserta/peserta';
	$PAGES['peserta-upload']['module'] = 'table, PHPMailerAutoload, ui, html2text, login, query';
	$PAGES['peserta-upload']['control'] = 'peserta/add_file';
	$PAGES['peserta-detail']['module'] = 'table, PHPMailerAutoload, ui, html2text, login, func_transfer';
	$PAGES['peserta-detail']['control'] = 'peserta/peserta';
	$PAGES['peserta-calon']['module'] = 'table, PHPMailerAutoload, ui, html2text, login, func_transfer';
	$PAGES['peserta-calon']['control'] = 'peserta/peserta_calon';
	$PAGES['cek_nip']['module'] = 'table, ui, func_transfer, login, query';
	$PAGES['cek_nip']['control'] = 'cek_nip';
	$PAGES['cek_diklat']['module'] = 'table, ui, func_transfer, login, query';
	$PAGES['cek_diklat']['control'] = 'cek_diklat';
	
	//evaluasi
	$PAGES['evaluasi']['module'] = 'table, ui, login, html2text, PHPExcel';
	$PAGES['evaluasi']['control'] = 'evaluasi/evaluasi';
	$PAGES['evaluasi_excel']['module'] = 'table, ui, login, html2text, PHPExcel';
	$PAGES['evaluasi_excel']['control'] = 'evaluasi/evaluasi_excel';
	$PAGES['evm']['module'] = 'table, ui, login, query';
	$PAGES['evm']['control'] = 'evaluasi/evaluasi_wi_manajemen';	
	
	//users
	$PAGES['users']['module'] = 'table, ui, login, html2text';
	$PAGES['users']['control'] = 'users/users';
	
	//reporting
	$PAGES['rep-jadwal-wi']['module'] = 'table, ui, login, html2text, PHPExcel';
	$PAGES['rep-jadwal-wi']['control'] = 'reporting/jadwal_wi';
	$PAGES['rep-jadwal-wi-2']['module'] = 'table, ui, login, html2text, PHPExcel';
	$PAGES['rep-jadwal-wi-2']['control'] = 'reporting/jadwal_wi_2';
	$PAGES['rep-pelatihan']['module'] = 'table, ui, login, html2text, PHPExcel';
	$PAGES['rep-pelatihan']['control'] = 'reporting/pelatihan';
	$PAGES['rep-not-evaluasi']['module'] = 'table, ui, login, html2text, PHPExcel';
	$PAGES['rep-not-evaluasi']['control'] = 'reporting/not_evaluasi';
	
	$PAGES['ajax-rep-pelatihan']['module'] = 'table, ui, login, html2text';
	$PAGES['ajax-rep-pelatihan']['control'] = 'reporting/ajax-pelatihan';
	
	$PAGES['pdf-kelas']['module'] = 'table, ui, login, html2text, tcpdf';
	$PAGES['pdf-kelas']['control'] = 'reporting/pdf_kelas';
	$PAGES['exc-peserta']['module'] = 'table, ui, login, html2text, PHPExcel';
	$PAGES['exc-peserta']['control'] = 'reporting/excel_kelas';
	$PAGES['pdf-absen']['module'] = 'table, ui, login, html2text, tcpdf';
	$PAGES['pdf-absen']['control'] = 'reporting/pdf_absen';
	
	$PAGES['pencarian']['module'] = 'table, ui, login, html2text';
	$PAGES['pencarian']['control'] = 'reporting/pencarian';
	$PAGES['rep-histori-pelatihan']['module'] = 'table, ui, login, html2text';
	$PAGES['rep-histori-pelatihan']['control'] = 'reporting/histori_pelatihan';
	$PAGES['rep-sertifikat']['module'] = 'table, ui, login, html2text, query';
	$PAGES['rep-sertifikat']['control'] = 'reporting/sertifikat_pelatihan';

	$PAGES['pdf-sertifikat']['module'] = 'table, ui, login, html2text, tcpdf, func_transfer, query';
	$PAGES['pdf-sertifikat']['control'] = 'reporting/pdf_sertifikat';
	$PAGES['cek-sertifikat']['module'] = 'table, ui, login, html2text, func_transfer';
	$PAGES['cek-sertifikat']['control'] = 'reporting/cek_sertifikat';
	$PAGES['pdf-keterangan']['module'] = 'table, ui, login, html2text, tcpdf, func_transfer';
	$PAGES['pdf-keterangan']['control'] = 'reporting/pdf_keterangan';
	$PAGES['cek-keterangan']['module'] = 'table, ui, login, html2text, func_transfer';
	$PAGES['cek-keterangan']['control'] = 'reporting/cek_keterangan';
	$PAGES['pdf-sertifikat-kosong']['module'] = 'table, ui, login, html2text, tcpdf, func_transfer, query';
	$PAGES['pdf-sertifikat-kosong']['control'] = 'reporting/pdf_sertifikat_kosong';
	$PAGES['pdf-keterangan-kosong']['module'] = 'table, ui, login, html2text, tcpdf, func_transfer';
	$PAGES['pdf-keterangan-kosong']['control'] = 'reporting/pdf_keterangan_kosong';
	$PAGES['pdf-sertifikat-pim']['module'] = 'table, ui, login, html2text, tcpdf, func_transfer, query';
	$PAGES['pdf-sertifikat-pim']['control'] = 'reporting/pdf_sertifikat_pim';
	$PAGES['pdf-sertifikat-latsar']['module'] = 'table, ui, login, html2text, tcpdf, func_transfer, query';
	$PAGES['pdf-sertifikat-latsar']['control'] = 'reporting/pdf_sertifikat_latsar';
	$PAGES['pdf-sertifikat-latsar-kosong']['module'] = 'table, ui, login, html2text, tcpdf, func_transfer, query';
	$PAGES['pdf-sertifikat-latsar-kosong']['control'] = 'reporting/pdf_sertifikat_latsar_kosong';
	$PAGES['pdf-sertifikat-latsartek']['module'] = 'table, ui, login, html2text, tcpdf, func_transfer, query';
	$PAGES['pdf-sertifikat-latsartek']['control'] = 'reporting/pdf_sertifikat_latsartek';
	$PAGES['pdf-sertifikat-latsartek-kosong']['module'] = 'table, ui, login, html2text, tcpdf, func_transfer, query';
	$PAGES['pdf-sertifikat-latsartek-kosong']['control'] = 'reporting/pdf_sertifikat_latsartek_kosong';

	$PAGES['rep-monev']['module'] = 'table, ui, login, html2text';
	$PAGES['rep-monev']['control'] = 'reporting/monev';
	$PAGES['rep-pe']['module'] = 'table, ui, login, html2text, PHPExcel';
	$PAGES['rep-pe']['control'] = 'reporting/rep_pe';
	$PAGES['rep-pusat-db-peserta']['module'] = 'table, ui, login, html2text, PHPExcel, query';
	$PAGES['rep-pusat-db-peserta']['control'] = 'reporting/rep_pusat_db_peserta';
	$PAGES['rep-pusat-kelengkapan']['module'] = 'table, ui, login, html2text, PHPExcel, query';
	$PAGES['rep-pusat-kelengkapan']['control'] = 'reporting/rep_pusat_kelengkapan_peserta';
	$PAGES['rep-pusat-eval-wi']['module'] = 'table, ui, login, html2text';
	$PAGES['rep-pusat-eval-wi']['control'] = 'reporting/rep_pusat_eval_wi';
	$PAGES['rep-pusat-eval-wi-detail']['module'] = 'table, ui, login, html2text, PHPExcel';
	$PAGES['rep-pusat-eval-wi-detail']['control'] = 'reporting/rep_pusat_excel_eval_wi';
	
	$PAGES['rep-eval-wi']['module'] = 'table, ui, login, html2text';
	$PAGES['rep-eval-wi']['control'] = 'reporting/eval_wi';
	$PAGES['rep-eval-wi-detail']['module'] = 'table, ui, login, html2text, PHPExcel';
	$PAGES['rep-eval-wi-detail']['control'] = 'reporting/excel_eval_wi';
	
	//WI_CENTER
	$PAGES['wi-upload']['module'] = 'table, PHPMailerAutoload, ui, html2text, login, query';
	$PAGES['wi-upload']['control'] = 'wi/add_file';

	//PIM
	$PAGES['mentor']['module'] = 'login, table, ui, html2text';
	$PAGES['mentor']['control'] = 'mentor/mentor';
	$PAGES['pim-berita']['module'] = 'login, table, ui, html2text';
	$PAGES['pim-berita']['control'] = 'pim/pp_news';
	$PAGES['peserta-mentor']['module'] = 'login, table, ui, html2text';
	$PAGES['peserta-mentor']['control'] = 'peserta/peserta_mentor';
	$PAGES['peserta-coach']['module'] = 'login, table, ui, html2text';
	$PAGES['peserta-coach']['control'] = 'peserta/peserta_coach';
	$PAGES['pim-peserta']['module'] = 'login, table, ui, html2text';
	$PAGES['pim-peserta']['control'] = 'pim/pim_peserta';
	$PAGES['add-comment-pim']['module'] = 'login,table';
	$PAGES['add-comment-pim']['control'] = 'pim/add-comment-pim';
	
	$PAGES['imp-tambahan']['module'] = 'login, table, ui, query';
	$PAGES['imp-tambahan']['control'] = 'imp_tambahan';
	
	//tmp
	$PAGES['exp']['module'] = 'table, ui, html2text';
	$PAGES['exp']['control'] = 'exp';
	
	//API
	$PAGES['allcourse']['module'] = 'table, ui, html2text';
	$PAGES['allcourse']['control'] = 'api/allcourse';
	$PAGES['course']['module'] = 'table, ui, html2text';
	$PAGES['course']['control'] = 'api/course';

	// PILIH ROUTE UTAMA
	$MAINPAGE = 'beranda';    

	///// ROUTING - SELESAI
	//////////////////////////////////////////////////////////////////////////////////////


	///////////////////////////////////////////////////////////////////////////////////////////////
	///// JANGAN DI EDIT
	$CONFIG['site']['view'] = "view/" . $CONFIG['site']['theme'] . "/";
	$CONFIG['site']['full'] = $CONFIG['site']['url'] . 'view/' . $CONFIG['site']['theme'] . '/';
    ///// JANGAN DI EDIT
	///////////////////////////////////////////////////////////////////////////////////////////////
?>