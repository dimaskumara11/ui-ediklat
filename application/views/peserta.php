<!DOCTYPE HTML>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
<title>Approval Certificate </title>
<link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/style.css">
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/framework.css">
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/fonts/css/fontawesome-all.min.css">    
</head>
    
<body class="theme-light bg-blue1-light" data-highlight="blue2">
    
<div id="page-preloader">  
    <div class="loader-main">
        <div class="preload-spinner preload-large border-highlight"></div>
    </div> 
</div>
<div class="preloader__"></div>
<div id="page" class="halaman">
    <div class="header header-fixed header-logo-app bg-blue1-light">
        <a href="<?=base_url()?>kelas" class="header-title" style="color: black">Back</a>
        <a href="<?=base_url()?>kelas" class="header-icon header-icon-1"><i style="color: black" class="fas fa-arrow-left"></i></a>
        <?php
        if($this->session->userdata('hak_akses') == 1)
        {
            if($status_approve_kapus==1)
            {
            ?>
                <a href="#" class="header-icon header-icon-4" data-menu="menu-forgot">
                    <button class="button button-full button-m shadow-large button-round-small top-10 bottom-0" style="cursor:pointer; background: yellow; color: blue">Approve</button>
                </a>
                <a href="#" class="header-icon header-icon-4" data-menu="menu-tolak" style="right: 180px">
                    <button class="button button-full button-m shadow-large button-round-small top-10 bottom-0" style="cursor:pointer; background: red; color: yellow;">Tolak</button>
                </a>
            <?php
            }
        }
        elseif($this->session->userdata('hak_akses') == 2)
        {
            if($status_approve_kaba==0)
            {
            ?>
                <a href="#" class="header-icon header-icon-4" data-menu="menu-forgot">
                    <button class="button button-full button-m shadow-large button-round-small top-10 bottom-0" style="cursor:pointer; background: yellow; color: blue">Approve</button>
                </a>
                <a href="#" class="header-icon header-icon-4" data-menu="menu-tolak" style="right: 180px">
                    <button class="button button-full button-m shadow-large button-round-small top-10 bottom-0" style="cursor:pointer; background: red; color: yellow;">Tolak</button>
                </a>
            <?php
            }
        }
        ?>
    </div>
        
    <div id="menu-1" class="menu menu-box-left" 
         data-menu-width="250"
         data-menu-effect="menu-over"
         data-menu-select="page-components">
         <?php $this->load->view('menu'); ?>
    </div>  
         

    <div class="page-content header-clear-medium">   
    <?php
    if($this->session->flashdata('alert') == "passphrase salah")
    {?>
        <div class="alert alert-large alert-round-medium bg-red2-dark alert_flash">
            <i class="fa fa-times-circle"></i>
            <strong class="uppercase ultrabold">Gagal, <br>Pin Yang Dimasukkan salah</strong>
            <span></span>
            <i class="fa fa-times"></i>
        </div>  
    <?php
    }
    else if($this->session->flashdata('alert') == "Password Salah")
    {?>
        <div class="alert alert-large alert-round-medium bg-red2-dark alert_flash">
            <i class="fa fa-times-circle"></i>
            <strong class="uppercase ultrabold">Sukses, <br>Password Salah</strong>
            <span></span>
            <i class="fa fa-times"></i>
        </div>  
    <?php
    }
    ?>
          
        <div class="content" style="color: black">
            <h2 class="bolder" align="center">Tabel Data Peserta</h2>
        </div>
        <div class="divider divider-margins"></div>
            <div class="row">
                <div class="col-md-12">
                    <table style="font-size: 17px;border: none">
                    <tr>
                        <td>
                            <b>Pelatihan : </b><br>
                            <?php 
                                ($q_kelas["id_diklat_versi"] > 0) ? $ver ='<span class="badge badge-danger">VERSI</span> ' : $ver = '';
                                echo $ver.$q_kelas["nama_diklat"];
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Tanggal Pelaksanaan : </b><br>
                            <?php echo date_indo($q_kelas["tgl_mulai"]).' s.d '.date_indo($q_kelas["tgl_akhir"])?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Nama WI :</b><br>
                            <?php
                                $wi = $this->db->select('nama')->join('wi_pengajar','wi_pengajar.id_pengajar = kelas_wi.id_wi')->get_where('kelas_wi',['id_kelas'=>$q_peserta[0]['id_kelas']])->result();
                                // echo $wi->nama;
                                foreach ($wi as $key => $value) 
                                {
                                    echo ucwords(strtolower($value->nama)).' , ';
                                }
                            ?>
                        </td>
                    </tr>
                    </table>
                </div>
            </div>
            <!-- <p>
                Swipe this table from your mobile device.
            </p> -->
        </div>
        <div class="table-scroll">          
            <table class="table-borders-dark bg-blue1-light" style="font-size: 18px">
                <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>NIP</th>
                    <th>Nilai</th>
                    <th>Status Kelulusan</th>
                    <th>Status</th>
                    <th></th>
                </tr>
                <?php
                $no = 1;
                foreach($q_peserta as $dt)
                {
                    $cek_status_generate = $this->db->select('status,file_pdf_digital,count(id) as total')->get_where('lock_ttd_digital',['id_kelas'=>$dt['id_kelas'],'noktp'=>$dt['noktp']])->row();
                    switch($dt["status_lulus"]){
                        case '0':
                            $status = 'Pending';
                            break;
                        case '1':
                            $status = 'Lulus';
                            break;
                        case '2':
                            $status = 'Tidak Lulus';
                            break;
                        case '3':
                            $status = 'Batal';
                            break;
                        default:
                            $status = '-';
                            break;
                    }
                    echo '<tr>
                        <td>'.$no.'</td>
                        <td>'.ucwords(strtolower($dt["nama"])).'</td>
                        <td>'.$dt["noktp"].'</td>
                        <td>'.$dt["nilai_akhir"].'</td>
                        <td>'.$status.'</td>
                        <td>';
                        if($cek_status_generate->status == 1)
                        {
                    ?>  
                        <span style="color: green">Berhasil</a>
                        </td>
                        <td>
                            <a target="_blank" href="<?=base_url()?>assets/pdf_digital/<?=$cek_status_generate->file_pdf_digital?>">
                                <button style="cursor:pointer" class="button button-3d border-green2-dark button-m shadow-large button-round-small bg-green1-dark bottom-0">Lihat Sertifikat Digital</button>
                            </a>
                    <?php 
                        }
                        else
                        {
                    ?>
                        <span style="color: red">Belum di TTD</a>
                        </td>
                        <td>
                            <a target="_blank" href="<?=base_url()?>Pdf_sertifikat/peserta/<?=$dt["id_kelas"]?>/<?=$dt["cert"]?>">
                                <button style="cursor:pointer" class="button button-3d border-blue2-dark button-m shadow-large button-round-small bg-highlight bottom-0">Lihat Draft Sertifikat</button>
                            </a>
                    <?php
                        }
                    echo 
                        '</td>
                        </tr>';
                    $no++;
                }
            ?>
            </table>    
        </div>
        
        <div class="divider divider-margins"></div>
    </div>
    
    <div id="menu-forgot" 
         class="menu menu-box-modal round-medium halaman" 
         data-menu-height="250" 
         data-menu-width="510" 
         data-menu-effect="menu-over">
        <div class="content">
            <div class="akses" id="<?=$this->session->userdata('hak_akses');?>"></div>
        <?php
        if($this->session->userdata('hak_akses')==1)
        {
        ?>
            <form action="<?=base_url()?>Approve/" method="post">
        <?php
        }
        elseif ($this->session->userdata('hak_akses')==2)
        {
        ?>
            <form action="<?=base_url()?>Cetak_digital/" method="post">
        <?php
        }
        ?>
                <h2 class="center-text uppercase ultrabold top-30">Forgot Password?</h2>
                <p class="center-text font-24 under-heading bottom-20" style="font-weight: bold">
                    Masukkan PIN Anda
                </p>
                    <input type="hidden" name="kelas" value="<?=$q_peserta[0]['id_kelas']?>">
                <div class="input-style has-icon input-style-2 input-required bottom-30">
                    <i class="input-icon fa fa-at"></i>
                    <span>PIN</span>
                    <em>(Harap dimasukkan)</em>
                    <input type="password" name="passphrase" placeholder="PIN" required="">
                </div>   
                <button id="approve_" style="width: 100%" class="button button-l shadow-large button-round-small bg-highlight">APPROVE</button>
            </form>
        </div>
    </div> 

    <div id="menu-2" class="menu menu-box-right" 
         data-menu-width="75"
         data-menu-effect="menu-over">
                    
        <div class="highlight-changer">
            <a href="#" data-change-highlight="red1"><i class="fa fa-circle color-red1-dark"></i><span class="color-red2-light">Red</span></a>    
            <a href="#" data-change-highlight="orange"><i class="fa fa-circle color-orange-dark"></i><span class="color-orange-light">Orange</span></a>    
            <a href="#" data-change-highlight="pink2"><i class="fa fa-circle color-pink2-dark"></i><span class="color-pink2-light">Pink</span></a>    
            <a href="#" data-change-highlight="magenta2"><i class="fa fa-circle color-magenta2-dark"></i><span class="color-magenta2-light">Purple</span></a>    
            <a href="#" data-change-highlight="blue2"><i class="fa fa-circle color-blue2-dark"></i><span class="color-blue2-light">Blue</span></a>
            <a href="#" data-change-highlight="aqua"><i class="fa fa-circle color-aqua-dark"></i><span class="color-aqua-light">Aqua</span></a>      
            <a href="#" data-change-highlight="teal"><i class="fa fa-circle color-teal-dark"></i><span class="color-teal-light">Teal</span></a>      
            <a href="#" data-change-highlight="mint"><i class="fa fa-circle color-mint-dark"></i><span class="color-mint-light">Mint</span></a>      
            <a href="#" data-change-highlight="green2"><i class="fa fa-circle color-green2-dark"></i><span class="color-green2-light">Green</span></a>    
            <a href="#" data-change-highlight="green1"><i class="fa fa-circle color-green1-dark"></i><span class="color-green1-light">Grass</span></a>       
            <a href="#" data-change-highlight="yellow2"><i class="fa fa-circle color-yellow2-dark"></i><span class="color-yellow2-light">Sunny</span></a>    
            <a href="#" data-change-highlight="yellow1"><i class="fa fa-circle color-yellow1-dark"></i><span class="color-yellow1-light">Goldish</span></a>
            <a href="#" data-change-highlight="brown1"><i class="fa fa-circle color-brown1-dark"></i><span class="color-brown1-light">Wood</span></a>    
            <a href="#" data-change-highlight="brown2"><i class="fa fa-circle color-brown2-dark"></i><span class="color-brown2-light">Earth</span></a>
            <a href="#" data-change-highlight="dark1"><i class="fa fa-circle color-dark1-dark"></i><span class="color-dark1-light">Night</span></a>
            <a href="#" data-change-highlight="dark2"><i class="fa fa-circle color-dark2-dark"></i><span class="color-dark2-light">Dark</span></a>
            <a href="#" data-change-highlight="gray2"><i class="fa fa-circle color-gray2-dark"></i><span class="color-gray2-light">Gray</span></a>
        </div>                        
    </div>    
    
    <div class="menu-hider"></div>
</div>

<script type="text/javascript" src="<?=base_url()?>assets/scripts/jquery.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/scripts/plugins.js" async></script>
<script type="text/javascript" src="<?=base_url()?>assets/scripts/custom.js" async></script>
<script type="text/javascript">
    setTimeout(function()
    { 
        $('.alert_flash').fadeOut('slow');
    }, 3000);
    $(document).ready(function()
    {
        $('#approve_').on('click',function()
        {
            $('.halaman').hide();
            var akses = $('.akses').attr('id');
            if(akses == 1)
            {
                $('.preloader__').html(`
                <div class="demo-preloader" style="margin:25% 15% 25% 35%;">
                    <div class="preload-spinner preload-large border-highlight"></div>
                    <br><br><br>
                    <h1 align="center"><b>Mohon Tunggu,<br> Proses Generate Sedang Berlangsung</b></h1>
                </div>
                `)
                $('body').click();
            }
            else if(akses == 2)
            {
                $('.preloader__').html(`
                <div class="demo-preloader" style="margin:25% 15% 25% 35%;">
                    <div class="preload-spinner preload-large border-highlight"></div>
                    <br><br><br>
                    <h1 align="center"><b>Mohon Tunggu,<br> Proses Generate Tanda Tangan Digital Sedang Berlangsung</b></h1>
                </div>
                `)
                $('body').click();
            }
        })
    })
</script>
</body>
