-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 01 Mar 2019 pada 03.42
-- Versi server: 10.1.37-MariaDB
-- Versi PHP: 5.6.39

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_ediklat`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `ttd_digital_history`
--

CREATE TABLE `ttd_digital_history` (
  `id` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `user` varchar(100) NOT NULL,
  `status` int(1) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `keterangan` text NOT NULL,
  `alasan` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ttd_digital_history`
--

INSERT INTO `ttd_digital_history` (`id`, `id_kelas`, `user`, `status`, `created`, `keterangan`, `alasan`) VALUES
(1, 51, 'kaba', 1, '2019-02-28 03:21:41', 'Tolak', 'tes'),
(2, 51, 'kapus', 2, '2019-02-28 03:23:42', 'approve', NULL),
(3, 51, 'kaba', 1, '2019-02-28 03:25:28', 'Tolak', 'teeessss'),
(4, 51, 'kapus', 2, '2019-02-28 03:30:14', 'approve', NULL),
(5, 51, 'kaba', 1, '2019-02-28 03:46:17', 'Tolak', 'testt'),
(6, 51, 'kapus', 2, '2019-02-28 03:50:07', 'approve', NULL),
(7, 51, 'kapus', 3, '2019-02-28 07:06:41', 'approve', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `ttd_digital_token`
--

CREATE TABLE `ttd_digital_token` (
  `id` int(11) NOT NULL,
  `token` text NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `id_pusat` int(2) NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ttd_digital_token`
--

INSERT INTO `ttd_digital_token` (`id`, `token`, `status`, `id_pusat`, `created`) VALUES
(1, '20ccc2766f897030931c00f4ade3ed2ae4b14bfc', 0, 14, '2019-02-28 06:43:23'),
(2, 'd79d0e9e8814d6a2c29d10a3c8ee94eca804234c', 0, 14, '2019-02-28 06:49:55'),
(3, '614e91283af51d5b982c204f81ff04f2c2dffca2', 0, 14, '2019-02-28 06:50:17'),
(4, '681b931c09db689e73c12721bc4ba2064d9441f9', 0, 14, '2019-02-28 06:50:42'),
(5, '916359d72875ae4a7de6a26b6ed2303f663de053', 0, 14, '2019-02-28 06:52:51');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `ttd_digital_history`
--
ALTER TABLE `ttd_digital_history`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `ttd_digital_token`
--
ALTER TABLE `ttd_digital_token`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `ttd_digital_history`
--
ALTER TABLE `ttd_digital_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `ttd_digital_token`
--
ALTER TABLE `ttd_digital_token`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
